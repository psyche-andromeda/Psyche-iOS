//
//  Timeline+CoreDataProperties.swift
//  Psyche
//
//  Created by Kevin Shim on 4/11/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//
//

import Foundation
import CoreData


extension Timeline {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Timeline> {
        return NSFetchRequest<Timeline>(entityName: "Timeline")
    }

    @NSManaged public var dateTime: NSDate?
    @NSManaged public var desc: String?
    @NSManaged public var name: String?
    @NSManaged public var phase: String?
    @NSManaged public var tmobj: TM_Media?

}
