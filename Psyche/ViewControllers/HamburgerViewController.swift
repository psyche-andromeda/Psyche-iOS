//
//  HamburgerViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/14/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class HamburgerViewController: PsycheViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.type = .Fade

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func missionClicked(_ sender: Any) {
        let vc = self.navigationController?.viewControllers[0] as! ViewController
        vc.destinationSection = .Mission
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func asteroidClicked(_ sender: Any) {
        let vc = self.navigationController?.viewControllers[0] as! ViewController
        vc.destinationSection = .Asteroid
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func socialClicked(_ sender: Any) {
        let vc = self.navigationController?.viewControllers[0] as! ViewController
        vc.destinationSection = .Social
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func spacecraftClicked(_ sender: Any) {
        let vc = self.navigationController?.viewControllers[0] as! ViewController
        vc.destinationSection = .Spacecraft
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
