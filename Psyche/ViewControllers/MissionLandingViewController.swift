//
//  MissionLandingViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/10/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class MissionLandingViewController: PsycheViewController {

    @IBOutlet var background: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Mission

        let leftrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(MissionLandingViewController.swipeRight(recognizer:)))
        leftrecognizer.direction = .right
        self.view.addGestureRecognizer(leftrecognizer)
        
        let rightrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(MissionLandingViewController.swipeLeft(recognizer:)))
        rightrecognizer.direction = .left
        self.view.addGestureRecognizer(rightrecognizer)
        
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(MissionLandingViewController.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func swipeRight(recognizer: UISwipeGestureRecognizer){
        print("swipe right")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func swipeLeft(recognizer: UISwipeGestureRecognizer){
        print("swipe left")
        
        self.performSegue(withIdentifier: "toTimeline", sender: self)
    }

    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "fadeTimeline", sender: self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTimeline" || segue.identifier == "fadeTimeline"{
            let timeline = segue.destination as! Timeline_ViewController
            timeline.missionViewController = self
            timeline.type = .Mission
        }
    }
    
    @objc override func presentHelp() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "helperVC") as! HelperViewController
        vc.helpTitle = "SWIPE TO EXPLORE THE MISSION"
        vc.helpBody = "Swipe left and right to learn more about the Psyche Mission."
        vc.arrows = .horizontal
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

