//
//  SocialMediaLandingPageViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/10/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit


class SocialMediaLandingPageViewController: PsycheViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.type = .Social
        let rightrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(SocialMediaLandingPageViewController.swipeLeft(recognizer:)))
        rightrecognizer.direction = .left
        self.view.addGestureRecognizer(rightrecognizer)
        
        let leftrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(recognizer:)))
        leftrecognizer.direction = .right
        self.view.addGestureRecognizer(leftrecognizer)
        
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)

        // Do any additional setup after loading the view.
    }
    
    @objc func swipeLeft(recognizer: UISwipeGestureRecognizer){
        print("swipe left")
        
        self.navigationController?.popViewController(animated: true)
    }
    @objc func swipeRight(recognizer: UISwipeGestureRecognizer){
        print("swipe left")
        
        self.performSegue(withIdentifier: "goToSocialMedia", sender: self)
    }
    
    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "fadeToSocialMedia", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
