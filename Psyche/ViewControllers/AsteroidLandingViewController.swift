//
//  AsteroidLandingViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/10/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit



class AsteroidLandingViewController: PsycheViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Asteroid //set section to Asteroid
        
        //swipe down gesture to pop to launch screen
        let bottomrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeUp(recognizer:)))
        bottomrecognizer.direction = .up
        self.view.addGestureRecognizer(bottomrecognizer)
        
        //swipe up gesture to push to size comparison landing
        let toprecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeDown(recognizer:)))
        toprecognizer.direction = .down
        self.view.addGestureRecognizer(toprecognizer)
        
        //tap to fade to size comparison
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)
        

    }
    
    //listener to push to size comparison
    @objc func swipeDown(recognizer: UISwipeGestureRecognizer){
        print("swipe down")
        
        self.performSegue(withIdentifier: "goToSizeComparison", sender: self)
    }
    
    //listener to fade to size comparison
    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "fadeToSizeComparison", sender: self)
    }
    
    //listener to pop to launch screen
    @objc func swipeUp(recognizer: UISwipeGestureRecognizer){
        print("swipe up")
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc override func presentHelp() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "helperVC") as! HelperViewController
        vc.helpTitle = "SWIPE TO EXPLORE THE ASTEROID"
        vc.helpBody = "Swipe up and down to learn more about the Psyche Asteroid."
        vc.arrows = .vertical
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
