//
//  ViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/10/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

enum Section{ //section of app
    case Asteroid
    case Mission
    case Social
    case Spacecraft
    case Fade
}

protocol UnwindType{ //protocol to get the section of VC
    func getType() -> Section
}
/* Override UIViewController to run setup code and contain instance variables used by all view controllers
 */
class PsycheViewController: UIViewController, UnwindType{
    var type = Section.Fade
    @IBOutlet weak var landingButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    var helpTimer = Timer()
    
    override var preferredStatusBarStyle: UIStatusBarStyle{ //set status bar text to white
        return .lightContent
    }
    @IBAction func fadeToHome(_ sender: Any) { //
        self.type = .Fade
        self.navigationController?.popToRootViewController(animated: true)
    }
    func getType() -> Section{
        return self.type //return section of view controller instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        
        helpTimer = Timer.scheduledTimer(timeInterval: 45, target: self, selector: #selector(PsycheViewController.presentHelp), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        helpTimer.invalidate()
    }
    
    @objc func presentHelp(){
        //overriden in actual view controllers
    }
    
    
}

class ViewController: PsycheViewController, UINavigationControllerDelegate {
    
    var destinationSection: Section? //section to segue to from hamburger menu
    
    //lablels for countdown
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    
    var timer = Timer()
    
    let launchDate = Date(timeIntervalSince1970: 1641020400) //date of launch in unix time
    var current = Date() //time of now
    var differenceSeconds = 0
    var month = 0, day = 0, min = 0, hr = 0, sec = 0
    let sysCalendar = NSCalendar.current

    
    override func viewDidAppear(_ animated: Bool) { //perform segue based on destination section
        if let destination = destinationSection{
            switch destination{
            case .Mission:
                destinationSection = nil
                self.performSegue(withIdentifier: "mission", sender: self)
            case .Asteroid:
                destinationSection = nil
                self.performSegue(withIdentifier: "asteroid", sender: self)
            case .Social:
                destinationSection = nil
                self.performSegue(withIdentifier: "social", sender: self)
            case .Spacecraft:
                destinationSection = nil
                self.performSegue(withIdentifier: "spacecraft", sender: self)
            case .Fade:
                print("fade")
                
            }
        }
        
    }
    @objc func updateTimer(){
        
        let unitFlags = Set<Calendar.Component>([.month, .day, .hour, .minute, .second]) //the components of countdown
        let thisMoment = Date()
        let differenceComponents = sysCalendar.dateComponents(unitFlags, from: thisMoment, to: launchDate) //difference from now to launch time seperated by components
        
        //update labels with new values
        monthLabel.text = String(describing: differenceComponents.month!)
        dayLabel.text = String(describing: differenceComponents.day!)
        minuteLabel.text = String(describing: differenceComponents.minute!)
        hourLabel.text = String(describing: differenceComponents.hour!)
        secondLabel.text = String(describing: differenceComponents.second!)
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.delegate = self //to set pop animations
        
        //swipedown for asteroid
        let toprecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeDown(recognizer:)))
        toprecognizer.direction = .down
        self.view.addGestureRecognizer(toprecognizer)
        
        //swipe left for mission
        let rightrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeLeft(recognizer:)))
        rightrecognizer.direction = .left
        self.view.addGestureRecognizer(rightrecognizer)
        
        //swipe up for spacecraft
        let bottomrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeUp(recognizer:)))
        bottomrecognizer.direction = .up
        self.view.addGestureRecognizer(bottomrecognizer)
        
        //swipe right for social media
        let leftrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeRight(recognizer:)))
        leftrecognizer.direction = .right
        self.view.addGestureRecognizer(leftrecognizer)
        
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.tap(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)
        
        updateTimer() //set timer values
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(ViewController.updateTimer)), userInfo: nil, repeats: true) //update timer every second

    }
    //handle swipe guestures and perform segues based on direction
    @objc func swipeDown(recognizer: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "asteroid", sender: self)
    }
    @objc func swipeLeft(recognizer: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "mission", sender: self)
    }
    @objc func swipeUp(recognizer: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "spacecraft", sender: self)
    }
    @objc func swipeRight(recognizer: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "social", sender: self)
    }
    @objc func tap(recognizer: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "openHelper", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openHelper"{
            let vc = segue.destination as! HelperViewController
            vc.helpTitle = "SWIPE TO EXPLORE"
            vc.helpBody = "Swipe directionally to learn more about ASU and NASA’s mission to a metal world."
        }
    }
    
    @objc override func presentHelp() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "helperVC") as! HelperViewController
        vc.helpTitle = "SWIPE TO EXPLORE"
        vc.helpBody = "Swipe directionally to learn more about ASU and NASA’s mission to a metal world."
        self.present(vc, animated: true, completion: nil)
    }
    
    //replace default navigation controller animations
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .pop{ //make sure navigation controller is about to pop a VC
            
            let vc = fromVC as! UnwindType //cast to protocol
            
            switch vc.getType(){
            case .Asteroid:
                return BottomToTopPopAnimator() //pop up
            case .Mission:
                return LeftToRightPopAnimator() //pop right
            case .Spacecraft:
                return TopToBottomPopAnimator() //pop down
            case .Social:
                return RightToLeftPopAnimator() //pop left
            case .Fade:
                return FadePopAnimator() //pop as fade
            }
            
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class BottomToTopPopAnimator:NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!//source/current VC
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)! //VC to pop to
        
        let screenHeight = UIScreen.main.bounds.size.height
        let screenWidth = UIScreen.main.bounds.size.width
        
        
        toVC.view.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: screenHeight) //position destination VC off right side of screen
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view) //put destination VC on screen
        
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in // set animation duration to 0.3 seconds matched above
            
            toVC.view.frame = (toVC.view.frame).offsetBy(dx: 0.0, dy: -screenHeight) //move VCs to left
            fromVC.view.frame = (fromVC.view.frame).offsetBy(dx: 0.0, dy: -screenHeight)
            
        }) { (Finished) -> Void in
            transitionContext.completeTransition(Finished) //finished animation
        }
        
    }
}
class TopToBottomPopAnimator:NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3 //duration of animation
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let screenHeight = UIScreen.main.bounds.size.height
        let screenWidth = UIScreen.main.bounds.size.width
        
        
        toVC.view.frame = CGRect(x: 0, y: -screenHeight, width: screenWidth, height: screenHeight)//postion VC above current view
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)//insert destination VC
        
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in // set animation duration to 0.3 seconds matched above
            
            toVC.view.frame = (toVC.view.frame).offsetBy(dx: 0.0, dy: screenHeight) //move VCs down
            fromVC.view.frame = (fromVC.view.frame).offsetBy(dx: 0.0, dy: screenHeight)
            
        }) { (Finished) -> Void in
            transitionContext.completeTransition(Finished) //animation finished
        }
        
    }
}

class LeftToRightPopAnimator:NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        toVC.view.frame = CGRect(x: -(screenWidth), y: 0, width: screenWidth, height: screenHeight)
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in // set animation duration
            
            toVC.view.frame = (toVC.view.frame).offsetBy(dx: screenWidth, dy: 0.0)
            fromVC.view.frame = (fromVC.view.frame).offsetBy(dx: fromVC.view.frame.size.width, dy: 0.0) // new screen strave from left to right
            
        }) { (Finished) -> Void in
            transitionContext.completeTransition(Finished)
        }
        
    }
}
class RightToLeftPopAnimator:NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        toVC.view.frame = CGRect(x: (screenWidth), y: 0, width: screenWidth, height: screenHeight)
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in // set animation duration
            
            toVC.view.frame = (toVC.view.frame).offsetBy(dx: -screenWidth, dy: 0.0)
            fromVC.view.frame = (fromVC.view.frame).offsetBy(dx: -screenWidth, dy: 0.0) // new screen strave from left to right
            
        }) { (Finished) -> Void in
            if toVC is SMListGridContainerViewController{
                let vc = toVC as! SMListGridContainerViewController
                vc.switchToView(type: .List)
            }
            transitionContext.completeTransition(Finished)
        }
        
    }
}
class FadePopAnimator:NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5 //duration of fade animation
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        
        toVC.view.alpha = 0.0 //set destination view to fully transparent
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)//insert destination view
        
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in // set animation duration
            
            toVC.view.alpha = 1.0 //set destination view to opague
            fromVC.view.alpha = 0.0 //set previous view to transparent
            
        }) { (Finished) -> Void in
            transitionContext.completeTransition(Finished) //finished animation
        }
        
    }
}



