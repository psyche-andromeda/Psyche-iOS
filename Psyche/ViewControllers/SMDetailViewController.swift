//
//  detailViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 12/13/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit
import Kingfisher

class SMDetailViewController: PsycheViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    var currentPost : SMPost?

    @IBOutlet weak var navBar: UIView!
    var lastContentOffset = CGFloat(0.0)
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let retweeted = currentPost!.repostedPost{
            currentPost = retweeted
        }
        
        collectionView.contentInset = UIEdgeInsets(top: self.view.safeAreaLayoutGuide.layoutFrame.minY+80, left: 0, bottom: 0, right: 0)
        UIApplication.shared.keyWindow!.sendSubview(toBack: self.view)
        UIApplication.shared.keyWindow!.bringSubview(toFront: collectionView)
        
        self.type = .Social

        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print (currentPost!.postImageLinks.count)
        return currentPost!.postImageLinks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var post = currentPost!
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "postHeader", for: indexPath) as! SMDetailHeaderReusableView
        
        if let retweeted = post.repostedPost{
            //cell.tagLineLabel.isHidden = false
            //cell.tagLineLabel.text = "\(post.name) Retweeted"
            post = retweeted
            
        } else{
            //cell.tagLineLabel.isHidden = true
            //cell.tagLineLabel.text = ""
        }
        
        cell.fullNameLabel.text = post.name
        cell.handleLabel.text = post.handle
        let length = post.post.length 
        cell.postTextLabel.attributedText = post.post.attributedSubstring(from: NSMakeRange(0, length))
        cell.postTextLabel.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : UIColor(named: "attributeColor"), NSAttributedStringKey.underlineColor.rawValue : UIColor(named: "attributeColor"), NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue]
        cell.postTextLabel.isUserInteractionEnabled = false
        cell.avatarImageView.kf.setImage(with: URL(string: post.profileImageLink))
        
        let unitFlags = Set<Calendar.Component>([.day, .hour, .minute, .second])
        let thisMoment = Date()
        let differenceComponents = NSCalendar.current.dateComponents(unitFlags, from: post.postDate, to: thisMoment)
        
        if differenceComponents.day! != 0{
            if differenceComponents.day! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.day!) + " day"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.day!) + " days"
            }
        } else if differenceComponents.hour! != 0 {
            if differenceComponents.hour! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.hour!) + " hr"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.hour!) + " hrs"
            }
        } else if differenceComponents.minute! != 0 {
            if differenceComponents.minute! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.minute!) + " min"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.minute!) + " mins"
            }
        } else if differenceComponents.second! != 0 {
            if differenceComponents.second! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.second!) + " sec"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.second!) + " secs"
            }
        }
        
        switch post.source{
        case .instagram:
            cell.smLogoImageView.image = UIImage(named: "instagramIcon")!
        case .twitter:
            cell.smLogoImageView.image = UIImage(named: "twitterIcon")!
        case .facebook:
            cell.smLogoImageView.image = UIImage(named: "facebookIcon")!
        case .blog:
            cell.smLogoImageView.image = UIImage(named: "instagramIcon")!
        case .embed:
            cell.smLogoImageView.image = nil
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgcell", for: indexPath) as! ImageCollectionViewCell
        
        cell.image.kf.setImage(with: URL(string: currentPost!.postImageLinks[indexPath.row] ))
        
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((self.lastContentOffset > scrollView.contentOffset.y && abs(self.lastContentOffset - scrollView.contentOffset.y) >= 80.0) || scrollView.contentOffset.y <= -80.0 ) {
            setView(view: self.navBar, hidden: false)
            
        }
        else if (self.lastContentOffset+20 < scrollView.contentOffset.y) {
            setView(view: self.navBar, hidden: true)
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        print(self.lastContentOffset)
        print("Scrolled")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -79.0{
            setView(view: self.navBar, hidden: false)
        }
        print (scrollView.contentOffset.y)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            if hidden{
                view.frame = CGRect(x: view.frame.minX, y: -120, width: view.frame.width, height: view.frame.height)
            } else{
                let guide = self.view.safeAreaLayoutGuide
                view.frame = CGRect(x: view.frame.minX, y: 0+guide.layoutFrame.minY, width: view.frame.width, height: view.frame.height)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
