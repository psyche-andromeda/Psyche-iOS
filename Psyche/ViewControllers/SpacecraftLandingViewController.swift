//
//  SpacecraftLandingViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/11/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class SpacecraftLandingViewController: PsycheViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Spacecraft
        
        let toprecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown(recognizer:)))
        toprecognizer.direction = .down
        self.view.addGestureRecognizer(toprecognizer)
        let bottomrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeUp(recognizer:)))
        bottomrecognizer.direction = .up
        self.view.addGestureRecognizer(bottomrecognizer)
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func swipeDown(recognizer: UISwipeGestureRecognizer){
        print("swipe down")
        self.navigationController?.popViewController(animated: true)
    }
    @objc func swipeUp(recognizer: UISwipeGestureRecognizer){
        print("swipe up")
        
        self.performSegue(withIdentifier: "goToAR", sender: self)
    }
    
    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "fadeToAR", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSpacecraftFacts" || segue.identifier == "fadeToSpacecraftFacts"{
            let vc = segue.destination as! FactsViewController
            vc.landingViewController = self
            vc.type = .Spacecraft
        }
    }
    
    @objc override func presentHelp() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "helperVC") as! HelperViewController
        vc.helpTitle = "SWIPE TO EXPLORE THE SPACECRAFT"
        vc.helpBody = "Swipe up and down to learn more about the Psyche Spacecraft."
        vc.arrows = .vertical
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
