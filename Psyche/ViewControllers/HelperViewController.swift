//
//  HelperViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/24/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

extension HelperViewController{
    enum Arrows{
        case horizontal
        case vertical
    }
}

class HelperViewController: UIViewController {
    
    var helpTitle: String?
    var helpBody: String?
    var arrows: Arrows?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var topArrow: UIImageView!
    @IBOutlet weak var leftArrow: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var bottomArrow: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(HelperViewController.dismissAction(recognizer:)))
        self.view.addGestureRecognizer(tapRecognizer)
        
        titleLabel.text = helpTitle!
        bodyLabel.text = helpBody!
        
        if let which = arrows{
            switch which{
            case .horizontal:
                topArrow.isHidden = true
                bottomArrow.isHidden = true
            case .vertical:
                leftArrow.isHidden = true
                rightArrow.isHidden = true
            }
        
        }

        // Do any additional setup after loading the view.
    }
    
    @objc func dismissAction(recognizer: UITapGestureRecognizer){
        self.dismiss(animated: true, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
