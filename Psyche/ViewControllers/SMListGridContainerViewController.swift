//
//  SMListGridContainerViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 1/10/18.
//  Copyright © 2018 Psyche Andromeda. All rights reserved.
//

import UIKit

protocol SMViewChangerDelegate {
    func switchToView(type: SMViewType)
}

enum SMViewType {
    case List
    case Grid
}

class SMListGridContainerViewController: PsycheViewController, SMViewChangerDelegate {
    
    private lazy var listViewController : SMListViewController = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        var vc = sb.instantiateViewController(withIdentifier: "SMListViewController") as! SMListViewController
        self.add(asChildViewController: vc)
        vc.changeDelegate = self
        
        return vc
    }()
    
    private lazy var gridViewController : SMGridViewController = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        var vc = sb.instantiateViewController(withIdentifier: "SMGridViewController") as! SMGridViewController
        self.add(asChildViewController: vc)
        vc.changeDelegate = self
        
        return vc
    }()
    
    var currentViewType : SMViewType = .List
    let bottomView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        let rightrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(SocialMediaLandingPageViewController.swipeLeft(recognizer:)))
        rightrecognizer.direction = .left
        self.view.addGestureRecognizer(rightrecognizer)
        self.type = .Social
        updateCurrentView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func swipeLeft(recognizer: UISwipeGestureRecognizer){
        print("swipe left")
        
        self.navigationController?.popViewController(animated: true)
    }
    private func add(asChildViewController vc : UIViewController){
        addChildViewController(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        
        bottomView.frame = UIScreen.main.bounds
        bottomView.backgroundColor = UIColor(red: 0.192, green: 0.125, blue: 0.271, alpha: 1.0)
        let window = UIApplication.shared.keyWindow!
        window.insertSubview(bottomView, at: 0)
        window.sendSubview(toBack: bottomView)
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
            ])
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            vc.view.topAnchor.constraintEqualToSystemSpacingBelow(guide.topAnchor, multiplier: 1.0),
            guide.bottomAnchor.constraintEqualToSystemSpacingBelow(vc.view.bottomAnchor, multiplier: 1.0)
            ])
        //vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vc.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController vc: UIViewController){
        //bottomView.removeFromSuperview()
        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()
    }
    
    func updateCurrentView(){
        switch currentViewType{
        case .List:
            remove(asChildViewController: gridViewController)
            add(asChildViewController: listViewController)
            
        case .Grid:
            remove(asChildViewController: listViewController)
            add(asChildViewController: gridViewController)
        }
    }
    
    func switchToView(type: SMViewType) {
        switch type {
        case .List:
            currentViewType = .List
        case .Grid:
            currentViewType = .Grid
        }
        updateCurrentView()
        print("Call change")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
