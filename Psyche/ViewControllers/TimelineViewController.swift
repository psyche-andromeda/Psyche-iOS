//
//  Timeline_ViewController.swift
//  Psyche
//
//  Created by Ogheneochukome Erhueh on 11/26/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class Timeline_ViewController: PsycheViewController , UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate{
    @IBOutlet weak var table: UITableView!
    
    var timeline = Timeline_model()
    var lastContentOffset = CGFloat(0.0)
    
    
    var missionViewController : MissionLandingViewController? = nil
    
    @IBOutlet weak var navBar: UIView!
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((self.lastContentOffset > scrollView.contentOffset.y && abs(self.lastContentOffset - scrollView.contentOffset.y) >= 80.0) || scrollView.contentOffset.y <= -80.0 ) {
            setView(view: self.navBar, hidden: false)
            
        }
        else if (self.lastContentOffset+20 < scrollView.contentOffset.y) {
            setView(view: self.navBar, hidden: true)
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        print(self.lastContentOffset)
        print("Scrolled")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -79.0{
            setView(view: self.navBar, hidden: false)
        }
        print (scrollView.contentOffset.y)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            if hidden{
                view.frame = CGRect(x: view.frame.minX, y: -80, width: view.frame.width, height: view.frame.height)
            } else{
                let guide = self.view.safeAreaLayoutGuide
                view.frame = CGRect(x: view.frame.minX, y: 0+guide.layoutFrame.minY, width: view.frame.width, height: view.frame.height)
            }
        })
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeline.getData().count
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        //cell.desc.text = ""
        // cell.year.text = "4004"
        
        
        let entry = timeline.get(index: indexPath.row)
        
        cell.desc.text = entry.name!
        cell.phase.text = entry.phase!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM yyyy"
        cell.year.text = formatter.string(from: entry.dateTime! as Date)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let label = UILabel()
        label.text = timeline.get(index: indexPath.row).name!
        label.numberOfLines = 0
        
        let maxSize = CGSize(width: 157, height: Int.max)
        let expectedSize = label.sizeThatFits(maxSize)
        print("\(indexPath.row) \(expectedSize.height)")
        return expectedSize.height + 20
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func gardfill() {
        self.view.backgroundColor = UIColor.clear
        
        let backgroundLayer = gradientColor().grad
        backgroundLayer.frame = self.view.frame
        self.view.layer.insertSublayer(backgroundLayer, at: 0)
        
        table.backgroundColor = UIColor.clear
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.separatorStyle = .none
        
        self.type = .Mission
        
        self.navBar.backgroundColor = UIColor(red:0.98, green:0.63, blue:0.00, alpha:1.0)
        self.gardfill()
        print(table.contentOffset.y)
        
        table.contentInset = UIEdgeInsets(top: self.view.safeAreaLayoutGuide.layoutFrame.minY+80, left: 0, bottom: 0, right: 0)
        
        let leftrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(recognizer:)))
        leftrecognizer.direction = .right
        self.view.addGestureRecognizer(leftrecognizer)
        
        let rightrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(Timeline_ViewController.swipeLeft(recognizer:)))
        rightrecognizer.direction = .left
        self.view.addGestureRecognizer(rightrecognizer)
        
        self.landingButton.removeTarget(nil, action: nil, for: .touchUpInside)
        self.landingButton.addTarget(nil, action: #selector(self.landingButtonPressed(_:)), for: .touchUpInside)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetail"
        {
            let vc = segue.destination as! Timeline1_ViewController
            vc.timelineItem = timeline.get(index: self.table.indexPathForSelectedRow!.row)
            table.deselectRow(at: self.table.indexPathForSelectedRow!, animated: false)
        }
        if segue.identifier == "timeLineToFacts"{
            let facts = segue.destination as! FactsViewController
            facts.landingViewController = self.missionViewController!
            facts.type = .Mission
        }
    }
    
    @objc func swipeRight(recognizer: UISwipeGestureRecognizer){
        print("swipe right")
        
        self.navigationController?.popViewController(animated: true)
    }
    @objc func swipeLeft(recognizer: UISwipeGestureRecognizer){
        print("swipe left")
        
        self.performSegue(withIdentifier: "timeLineToFacts", sender: self)
    }
    
    
    @objc func landingButtonPressed(_ sender: Any) {
        self.type = .Fade
        self.navigationController?.popToViewController(missionViewController!, animated: true)
    }
    
    @objc override func presentHelp() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "helperVC") as! HelperViewController
        vc.helpTitle = "EXPLORE THE TIMELINE"
        vc.helpBody = "Tap on any item to view details about each phase of the mission. \nKeep swiping to view facts about the mission as a whole."
        vc.arrows = .horizontal
        self.present(vc, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




