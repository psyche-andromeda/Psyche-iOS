//
//  ARLandingViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/18/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class ARLandingViewController: PsycheViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Spacecraft
        
        // Do any additional setup after loading the view.
        
        let bottomrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeUp(recognizer:)))
        bottomrecognizer.direction = .up
        self.view.addGestureRecognizer(bottomrecognizer)
        
        let toprecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeDown(recognizer:)))
        toprecognizer.direction = .down
        self.view.addGestureRecognizer(toprecognizer)
        
        
        
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSpacecraftFacts" {
            let vc = segue.destination as! FactsViewController
            vc.landingViewController = self
            vc.type = .Spacecraft
        }
    }
    @objc func swipeDown(recognizer: UISwipeGestureRecognizer){
        print("swipe down")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "goToARTool", sender: self)
    }
    
    @objc func swipeUp(recognizer: UISwipeGestureRecognizer){
        print("swipe up")
        self.performSegue(withIdentifier: "goToSpacecraftFacts", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
