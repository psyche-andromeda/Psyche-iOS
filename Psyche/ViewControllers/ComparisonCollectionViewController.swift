//
//  ComparisonCollectionViewController.swift
//  ParallaxPageView
//
//  Created by Jan Chen on 3/16/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit
import SpriteKit

//protocol to change nodes on SKScene
protocol sceneManagerDelegate {
    func changeObject(objectNode: ComparisonObject)
}

class ComparisonCollectionViewController: PsycheViewController, UICollectionViewDataSource, UICollectionViewDelegate {
   
    var comparisonObjects = ComparisonObject.getComparisonObjects() //load objects being compared
    
    @IBOutlet weak var skView: SKView!
    var comparisonScene = ComparisonScene() //SKScene that displays objects at scale
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        comparisonScene = ComparisonScene(size: skView.frame.size) //set scene size
        comparisonScene.scaleMode = .aspectFill //set scaling to fill screen but keep aspect ratio
        skView.showsFPS = true //debug framerate
        skView.showsNodeCount = true //debug node count
        skView.ignoresSiblingOrder = true //
        skView.presentScene(comparisonScene) //present scene
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.comparisonObjects.count //return number of comparison objects
    }
    
    //set comparison objects as collection view cells
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "objectcell", for: indexPath) as! ComparisonObjectTableViewCell
        cell.comparisonObject = comparisonObjects[indexPath.item]
        return cell
    }
    
    //tell SKScene to change comparison object based on what is selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("click")
        let sceneDelegate = comparisonScene
        sceneDelegate.changeObject(objectNode: comparisonObjects[indexPath.item])
    }
    
    //pop to go back to size comparison landing
    @IBAction func closeClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
