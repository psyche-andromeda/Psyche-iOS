//
//  GalleryViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/18/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class GalleryViewController: PsycheViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navBar: UIView!
    var lastContentOffset = CGFloat(0.0)
    var images = Gallery.getGallery()
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func homeClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToFull"{
            let vc = segue.destination as! FullImageViewController
            vc.picture = images[self.collectionView.indexPath(for: sender as! UICollectionViewCell)!.row]
            
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        //return SMPosts.posts.count
        
        
        
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! ImageCollectionViewCell
        cell.image.image = images[indexPath.row].image
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width  = self.collectionView.frame.size.width;
        
        return CGSize(width: width*0.33-8, height: width*0.33-8)
    }
    

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((self.lastContentOffset > scrollView.contentOffset.y && abs(self.lastContentOffset - scrollView.contentOffset.y) >= 80.0) || scrollView.contentOffset.y <= -80.0 ) {
            setView(view: self.navBar, hidden: false)
            
        }
        else if (self.lastContentOffset+20 < scrollView.contentOffset.y) {
            setView(view: self.navBar, hidden: true)
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        print(self.lastContentOffset)
        print("Scrolled")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -79.0{
            setView(view: self.navBar, hidden: false)
        }
        print (scrollView.contentOffset.y)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            if hidden{
                view.frame = CGRect(x: view.frame.minX, y: -80, width: view.frame.width, height: view.frame.height)
            } else{
                let guide = self.view.safeAreaLayoutGuide
                view.frame = CGRect(x: view.frame.minX, y: 0+guide.layoutFrame.minY, width: view.frame.width, height: view.frame.height)
            }
        })
    }
    
    
}

