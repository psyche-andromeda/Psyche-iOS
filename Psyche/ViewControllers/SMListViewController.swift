//
//  listViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 12/14/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit
import Kingfisher


private let reuseIdentifier = "SMListCell"

enum source {
    case twitter
    case facebook
    case all
}

class SMListViewController: PsycheViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    var changeDelegate: SMViewChangerDelegate?
    var sources: source = .all
    var posts = OrderedList<SMPost>()

    @IBOutlet weak var navBar: UIView!
    var lastContentOffset = CGFloat(0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //collectionView.register(UINib.init(nibName: "FeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SMListCell")
        //collectionView.register(UINib.init(nibName: "CardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CardCell")
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
            
        }
        
        collectionView.contentInset = UIEdgeInsets(top: self.view.safeAreaLayoutGuide.layoutFrame.minY+80, left: 0, bottom: 0, right: 0)
        
        self.type = .Social
        
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
 //       self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
//
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController!.setNavigationBarHidden(true, animated: false)
        if SMPosts.posts.count == 0{
            SMPosts.getTwitterPosts {
                self.collectionView.reloadData()
                print("finished loading")
            
            }
        }
//        SMPosts.getFacebookPosts {
//            self.collectionView.reloadData()
//            print("finished loading FB")
//
//        }

        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        
        self.posts = SMPosts.posts
        return SMPosts.posts.count
        
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCollectionViewCell
//
//        var post = SMPosts.posts[indexPath.row]
//
//        cell.card.subtitle = post.post.string
//
//        return cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FeedCollectionViewCell
        var post = self.posts[indexPath.row]
        if let retweeted = post.repostedPost{
            cell.tagLineLabel.isHidden = false
            cell.tagLineLabel.text = "\(post.name) Retweeted"
            post = retweeted

        } else{
            cell.tagLineLabel.isHidden = true
            cell.tagLineLabel.text = ""
        }

        cell.fullNameLabel.text = post.name
        cell.handleLabel.text = post.handle
        let length = post.post.length < 140 ? post.post.length : 140
        cell.postTextLabel.attributedText = post.post.attributedSubstring(from: NSMakeRange(0, length))
        cell.postTextLabel.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : UIColor(named: "attributeColor"), NSAttributedStringKey.underlineColor.rawValue : UIColor(named: "attributeColor"), NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue]
        cell.postTextLabel.isUserInteractionEnabled = false
        cell.avatarImageView.kf.setImage(with: URL(string: post.profileImageLink))

        let unitFlags = Set<Calendar.Component>([.day, .hour, .minute, .second])
        let thisMoment = Date()
        let differenceComponents = NSCalendar.current.dateComponents(unitFlags, from: post.postDate, to: thisMoment)

        if differenceComponents.day! != 0{
            if differenceComponents.day! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.day!) + " day"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.day!) + " days"
            }
        } else if differenceComponents.hour! != 0 {
            if differenceComponents.hour! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.hour!) + " hr"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.hour!) + " hrs"
            }
        } else if differenceComponents.minute! != 0 {
            if differenceComponents.minute! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.minute!) + " min"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.minute!) + " mins"
            }
        } else if differenceComponents.second! != 0 {
            if differenceComponents.second! == 1{
                cell.dateLabel.text = String(describing: differenceComponents.second!) + " sec"
            } else{
                cell.dateLabel.text = String(describing: differenceComponents.second!) + " secs"
            }
        }

        if post.postImageLinks.count > 0{
            cell.imageHeight.constant = 220
            cell.postImage.isHidden = false
            cell.postImage.kf.setImage(with: URL(string: post.postImageLinks[0]))
            cell.postImage.frame.size.height = 200
        } else{
            cell.imageHeight.constant = 0
            cell.postImage.isHidden = true
        }

        switch post.source{
        case .instagram:
                cell.smLogoImageView.image = UIImage(named: "instagramIcon")!
        case .twitter:
            cell.smLogoImageView.image = UIImage(named: "twitterIcon")!
        case .facebook:
            cell.smLogoImageView.image = UIImage(named: "facebookIcon")!
        case .blog:
            cell.smLogoImageView.image = UIImage(named: "instagramIcon")!
        case .embed:
            cell.smLogoImageView.image = nil
        }


        // Configure the cell

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SMHeader", for: indexPath) as! SMListHeaderView
        
        headerView.sourceSegmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 16)!], for: .normal)
        headerView.sourceSegmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor(named: "attributeColor")!, NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 16)!], for: .selected)
        headerView.sourceSegmentedControl.tintColor = UIColor.white
        
        headerView.sourceSegmentedControl.layer.cornerRadius = 11.0;
        headerView.sourceSegmentedControl.layer.borderColor = UIColor.white.cgColor;
        headerView.sourceSegmentedControl.layer.borderWidth = 1.0;
        headerView.sourceSegmentedControl.layer.masksToBounds = true;
        
        headerView.sourceSegmentedControl.selectedSegmentIndex = 1
        headerView.sourceSegmentedControl.setEnabled(false, forSegmentAt: 0)
        
        return headerView
        
    }
    
    
    @IBAction func gridToggleClicked(_ sender: Any) {
        changeDelegate!.switchToView(type: .Grid)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "expandListCell"{
            let vc = segue.destination as! SMDetailViewController
            vc.currentPost = self.posts[(self.collectionView.indexPath(for: (sender as! FeedCollectionViewCell))?.row)!]
            vc.type = .Social
        }
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl){
        print("start refreshing")
        SMPosts.posts.removeAll()
        SMPosts.getTwitterPosts{
            self.collectionView.reloadData()
            sender.endRefreshing()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((self.lastContentOffset > scrollView.contentOffset.y && abs(self.lastContentOffset - scrollView.contentOffset.y) >= 80.0) || scrollView.contentOffset.y <= -80.0 ) {
            setView(view: self.navBar, hidden: false)
            
        }
        else if (self.lastContentOffset+20 < scrollView.contentOffset.y) {
            setView(view: self.navBar, hidden: true)
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        print(self.lastContentOffset)
        print("Scrolled")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -79.0{
            setView(view: self.navBar, hidden: false)
        }
        print (scrollView.contentOffset.y)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            if hidden{
                view.frame = CGRect(x: view.frame.minX, y: -120, width: view.frame.width, height: view.frame.height)
            } else{
                let guide = self.view.safeAreaLayoutGuide
                view.frame = CGRect(x: view.frame.minX, y: 0+guide.layoutFrame.minY, width: view.frame.width, height: view.frame.height)
            }
        })
    }
}

extension SMListViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        if section != 0{
            return CGSize.zero
        }else{
            return CGSize(width: collectionView.bounds.size.width, height: 120.0)
        }
    }
}



