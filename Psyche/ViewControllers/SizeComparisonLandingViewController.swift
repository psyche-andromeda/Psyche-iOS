//
//  SizeComparisonLandingViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/18/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class SizeComparisonLandingViewController: PsycheViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Asteroid
        
        // Do any additional setup after loading the view.
        
        //swipe up to pop to landing page
        let bottomrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeUp(recognizer:)))
        bottomrecognizer.direction = .up
        self.view.addGestureRecognizer(bottomrecognizer)
        
        //swipe down to asteroid fact
        let toprecognizer = UISwipeGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.swipeDown(recognizer:)))
        toprecognizer.direction = .down
        self.view.addGestureRecognizer(toprecognizer)
        
        //tap to enter size comparioson tool
        let taprecognizer = UITapGestureRecognizer(target: self, action: #selector(AsteroidLandingViewController.tapRecognizer(recognizer:)))
        self.view.addGestureRecognizer(taprecognizer)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //set section for facts before segueing to it
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAsteroidFacts" || segue.identifier == "fadeToAsteroidFacts"{
            let vc = segue.destination as! FactsViewController
            vc.landingViewController = self
            vc.type = .Asteroid
        }
    }
    
    //gesture listener to push facts vc
    @objc func swipeDown(recognizer: UISwipeGestureRecognizer){
        print("swipe down")
        
        self.performSegue(withIdentifier: "goToAsteroidFacts", sender: self)
    }
    
    //gesture listener to enter size comparison tool
    @objc func tapRecognizer(recognizer: UISwipeGestureRecognizer){
        print("tap")
        
        self.performSegue(withIdentifier: "goToSizeComparisonTool", sender: self)
    }
    
    //gesture listener to pop back to asteroid landing page
    @objc func swipeUp(recognizer: UISwipeGestureRecognizer){
        print("swipe up")
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
