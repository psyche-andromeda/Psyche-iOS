//
//  FullImageViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 4/18/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class FullImageViewController: PsycheViewController {

    @IBOutlet weak var image: UIImageView!
    
    var picture : Picture? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.image.image = picture?.image
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homePressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
