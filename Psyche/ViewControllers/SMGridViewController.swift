//
//  gridViewController.swift
//  Psyche
//
//  Created by Kevin Shim on 12/13/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SMListCell"

enum MediaType{
    case Image
    case Video
}

struct MediaLink{
    var type : MediaType
    var link : String
    var post : SMPost
}

class SMGridViewController: PsycheViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    var changeDelegate : SMViewChangerDelegate?
    
    @IBOutlet weak var navBar: UIView!
    var lastContentOffset = CGFloat(0.0)

    @IBOutlet weak var collectionView: UICollectionView!
    
    var imgLinks : [MediaLink] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        
        self.type = .Social
        
        collectionView.contentInset = UIEdgeInsets(top: self.view.safeAreaLayoutGuide.layoutFrame.minY+80, left: 0, bottom: 0, right: 0)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if SMPosts.posts.count == 0{
            SMPosts.getTwitterPosts {
                self.collectionView.reloadData()
                print("finished loading")
                
            }
        }
//        if SMPosts.posts.count == 0{
//            self.collectionView.contentOffset = CGPoint(x: 0, y: -self.collectionView.refreshControl!.frame.size.height)
//
//            collectionView.refreshControl!.beginRefreshing()
//            refreshOptions(sender: collectionView.refreshControl!)
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc private func refreshOptions(sender: UIRefreshControl){
        print("start refreshing")
        SMPosts.posts.removeAll()
        self.imgLinks.removeAll()
        SMPosts.getTwitterPosts{
            self.collectionView.reloadData()
            sender.endRefreshing()
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        //return SMPosts.posts.count
        
        for post in SMPosts.posts{
            if let repostedPost = post.repostedPost{
                for link in repostedPost.postImageLinks{
                    imgLinks.append(MediaLink(type: .Image, link: link, post: repostedPost))
                }
            } else{
                for link in post.postImageLinks{
                    imgLinks.append(MediaLink(type: .Image, link: link, post: post))
                }
                
            }
        }
        
        return imgLinks.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! ImageCollectionViewCell
        cell.image.kf.setImage(with: URL(string: imgLinks[indexPath.row].link))
        return cell
        
    }

    // MARK: UICollectionViewDelegate
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SMHeader", for: indexPath) as! SMListHeaderView
        
        headerView.sourceSegmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 16)!], for: .normal)
        headerView.sourceSegmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor(named: "attributeColor")!, NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 16)!], for: .selected)
        headerView.sourceSegmentedControl.tintColor = UIColor.white
        
        headerView.sourceSegmentedControl.layer.cornerRadius = 11.0;
        headerView.sourceSegmentedControl.layer.borderColor = UIColor.white.cgColor;
        headerView.sourceSegmentedControl.layer.borderWidth = 1.0;
        headerView.sourceSegmentedControl.layer.masksToBounds = true;
        
        headerView.sourceSegmentedControl.selectedSegmentIndex = 1
        headerView.sourceSegmentedControl.setEnabled(false, forSegmentAt: 0)
        
        
        return headerView
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width  = self.collectionView.frame.size.width;
        
        return CGSize(width: width*0.33-8, height: width*0.33-8)
    }
    
    @IBAction func listToggleClicked(_ sender: Any) {
        changeDelegate!.switchToView(type: .List)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "expandImgCell"{
            let vc = segue.destination as! SMDetailViewController
            vc.currentPost = self.imgLinks[(self.collectionView.indexPath(for: (sender as! ImageCollectionViewCell))?.row)!].post
            vc.type = .Social
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((self.lastContentOffset > scrollView.contentOffset.y && abs(self.lastContentOffset - scrollView.contentOffset.y) >= 80.0) || scrollView.contentOffset.y <= -80.0 ) {
            setView(view: self.navBar, hidden: false)
            
        }
        else if (self.lastContentOffset+20 < scrollView.contentOffset.y) {
            setView(view: self.navBar, hidden: true)
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        print(self.lastContentOffset)
        print("Scrolled")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -79.0{
            setView(view: self.navBar, hidden: false)
        }
        print (scrollView.contentOffset.y)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            if hidden{
                view.frame = CGRect(x: view.frame.minX, y: -120, width: view.frame.width, height: view.frame.height)
            } else{
                let guide = self.view.safeAreaLayoutGuide
                view.frame = CGRect(x: view.frame.minX, y: 0+guide.layoutFrame.minY, width: view.frame.width, height: view.frame.height)
            }
        })
    }
    

}
extension GalleryViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        if section != 0{
            return CGSize.zero
        }else{
            return CGSize(width: collectionView.bounds.size.width, height:120.0)
        }
    }
}
