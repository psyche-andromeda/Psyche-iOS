//
//  Timeline1_ViewController.swift
//  Psyche
//
//  Created by Ogheneochukome Erhueh on 1/16/18.
//  Copyright © 2018 Psyche Andromeda. All rights reserved.
//

import UIKit

//detail view for timeline

class Timeline1_ViewController: PsycheViewController{
    
    @IBOutlet weak var TMitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var TMdescription: UILabel!
    @IBOutlet weak var TMdateLabel: UILabel!
    
    var timelineItem: Timeline? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Mission
        
        //set gradient background
        let backgroundLayer = gradientColor().grad
        backgroundLayer.frame = self.view.frame
        self.view.layer.insertSublayer(backgroundLayer, at: 0)
        
        //set information
        TMitle.text = timelineItem!.name
        TMdescription.text = timelineItem!.desc
        
        //set milestone time
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        TMdateLabel.text = formatter.string(from: timelineItem!.dateTime! as Date)
        
        //set pop gesture when swiping right
        let leftrecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(recognizer:)))
        leftrecognizer.direction = .right
        self.view.addGestureRecognizer(leftrecognizer)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //back button handler pops to timeline tableview
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //swipe right handler pops to timeline tableview
    @objc func swipeRight(recognizer: UISwipeGestureRecognizer){
        print("swipe right")
        
        self.navigationController?.popViewController(animated: true)
    } 
    
    
}
