//
//  TableViewCell.swift
//  Psyche
//
//  Created by Ogheneochukome Erhueh on 3/16/18.
//  Copyright © 2018 Psyche Andromeda. All rights reserved.
//

import UIKit

//Timeline's tableview cell

class TableViewCell: UITableViewCell {

    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var phase: UILabel!
    @IBOutlet weak var year: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
