//
//  TestCollectionViewCell.swift
//  ParallaxPageView
//
//  Created by Kevin Shim on 2/8/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

//collectionview cell for facts 

class TestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak internal var sectionLabel: UILabel!
    @IBOutlet weak var slideImageView: UIImageView!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var topSpaceContraint: NSLayoutConstraint!
    
    var slideCount: Int = 0
    var currentSlideIndex: Int = 0
    
    var slideshow: Slideshow? {
        didSet {
            self.displaySlides()
        }
    }
    
    private func displaySlides() {
        if let slideshow = slideshow {
            sectionLabel.text = sectionLabel.text?.uppercased()
            sectionLabel.text = slideshow.sectionTitle
            titleLabel.text = slideshow.slideTitle
            detailTextView.text = slideshow.slideDescription
            slideImageView.image = slideshow.slideImage
        }
        else {
            sectionLabel.text = ""
            titleLabel.text = ""
            detailTextView.text = ""
        }
    }
    
    
}
