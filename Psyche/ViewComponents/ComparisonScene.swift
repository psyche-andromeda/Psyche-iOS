//
//  SizeComparisonScene.swift
//  ParallaxPageView
//
//  Created by Jan Chen on 3/8/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit
import SpriteKit

/*
 Sprite Kit scene to display objects to compare the relative size of Psyche with recognizeable objects
 */

class ComparisonScene: SKScene, sceneManagerDelegate {
  
    let asteroidNode = SKSpriteNode(imageNamed: "Psyche") //asteroid sprite
    
    let iconBgNode = SKSpriteNode(imageNamed: "compicon_bg")
    let newNode = SKSpriteNode(imageNamed: "Earth")
    
    //magnifying glass scene
    let magnifyingGlass = SKShapeNode(circleOfRadius: 52)
    let magnifyingStem = SKShapeNode(rectOf: CGSize(width: 2, height: 34))
    var line = SKShapeNode()
    
    
    let asteroidBigNode = SKSpriteNode(imageNamed: "Psyche")
    
    //setup scene
    override func didMove(to view: SKView) {
        //set scene background
        backgroundColor = UIColor(red:0.07, green:0.01, blue:0.11, alpha:1.0)
        
        //draw initial asteroid
        asteroidNode.name = "psyche"
        asteroidNode.position = CGPoint(x: 0, y: 0)
        asteroidNode.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        asteroidNode.size = CGSize(width: 110, height: 100)
        asteroidNode.zPosition = 3
        addChild(asteroidNode)
        
        //draw inital comparison object
        newNode.name = "compobject"
        newNode.position = CGPoint(x: 0, y: 0)
        newNode.size = CGSize(width: 600.0, height: 600.0)
        newNode.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        newNode.zPosition = 1
        addChild(newNode)
        
        //draw initial magnifying glass off screen
        magnifyingGlass.fillColor = UIColor(red: 45, green: 32, blue: 64, alpha: 1)
        magnifyingGlass.strokeColor = UIColor.white
        magnifyingGlass.position = CGPoint(x: 0, y: -104)
        magnifyingGlass.zPosition = 3
        addChild(magnifyingGlass)
        
        //draw Psyche magnifyed off screen
        asteroidBigNode.name = "psycheBig"
        asteroidBigNode.anchorPoint = CGPoint(x: 0, y: 1)
        asteroidBigNode.position = CGPoint(x: 0, y: 0)
        asteroidBigNode.size = CGSize(width: 76, height: 68)
        asteroidBigNode.zPosition = 3
        addChild(asteroidBigNode)
        
        
    }
    
    func changeObject(objectNode: ComparisonObject) {
        if let node = childNode(withName: "compobject") as! SKSpriteNode? {
            
            //earth selected
            if(objectNode.objectName == "Earth") {
                
                //clear magnifying line to prepare redraw
                line.removeFromParent()
                
                node.texture = SKTexture(imageNamed: "Earth")//set comparison object's texture to earth
                let moveEarth = SKAction.move(to: CGPoint(x: -36, y: 1075), duration: 0.5) //set final position of earth
                let resizeEarth = SKAction.resize(toWidth: 824, height: 824, duration: 0.5) //set final size of earth
                let earthActions = SKAction.group([moveEarth, resizeEarth]) //group to earth's actions for final state
                node.run(earthActions) //animate to earth's final state
                
                let moveAsteroid = SKAction.move(to: CGPoint(x: 57, y: 307), duration: 0.5) //set asteroid's position
                let resizeAsteroid = SKAction.resize(toWidth: 18, height: 16, duration: 0.5) //set asteroid's final size
                let asteroidActions = SKAction.group([moveAsteroid, resizeAsteroid]) //set asteroid's action for final state
                asteroidNode.run(asteroidActions) //animate to asteroid's final state
                
                //position asteroid to center of magnifying glass's final position
                asteroidBigNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                let moveBigAsteroid = SKAction.move(to:CGPoint(x: 80, y: 200), duration:0.5)
                asteroidBigNode.run(moveBigAsteroid)

                //position and animate magnifying glass
                let moveMagnifying = SKAction.move(to: CGPoint(x: 80, y: 200), duration: 0.5)
                magnifyingGlass.run(moveMagnifying)
                
                //draw magnifying glass line
                var points = [CGPoint(x: 63, y: 299), CGPoint(x: 80, y: 200)]
                line = SKShapeNode(points: &points, count: points.count)
                line.lineWidth = 3
                addChild(line)

                iconBgNode.isHidden = true
            }
                
            //moon selected
            else if(objectNode.objectName == "Moon") {
                
                //clear magnifying line to prepare redraw
                line.removeFromParent()
                
                //actions to move and size asteroid for moon
                let moveAsteroid = SKAction.move(to: CGPoint(x: 52, y: 396), duration: 0.5)
                let resizeAsteroid = SKAction.resize(toWidth: 30, height: 27, duration: 0.5)
                let asteroidActions = SKAction.group([moveAsteroid, resizeAsteroid])
                asteroidNode.run(asteroidActions)
                
                //set comparison object to moon, size it, position it, animate
                node.texture = SKTexture(imageNamed: "Moon")
                let moveMoon = SKAction.move(to: CGPoint(x: 88, y: 721), duration: 0.5)
                let resizeMoon = SKAction.resize(toWidth: 375, height: 375, duration: 0.5)
                let moonActions = SKAction.group([moveMoon, resizeMoon])
                print(node.position)
                node.run(moonActions)
                
                //set asteroid inside magnifying glass
                asteroidBigNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                let moveBigAsteroid = SKAction.move(to:CGPoint(x: 80, y: 200), duration:0.5)
                asteroidBigNode.run(moveBigAsteroid)
                
                //position magnifying glass
                let moveMagnifying = SKAction.move(to: CGPoint(x: 80, y: 200), duration: 0.5)
                magnifyingGlass.run(moveMagnifying)
                
                //draw magnifying glass line
                var points = [CGPoint(x: 67, y: 383), CGPoint(x: 80, y: 200)]
                line = SKShapeNode(points: &points, count: points.count)
                line.lineWidth = 3
                addChild(line)
            }
           
            //empire state building selected
            else if(objectNode.objectName == "Empire State") {
                //clear magnifying line to prepare redraw
                line.removeFromParent()
                
                //set position and size of asteroid and animate to that state
                let moveAsteroid = SKAction.move(to: CGPoint(x: -11000, y: 18200), duration: 0.5)
                let resizeAsteroid = SKAction.resize(toWidth: 21625, height: 18000, duration: 0.5)
                let asteroidActions = SKAction.group([moveAsteroid, resizeAsteroid])
                asteroidNode.run(asteroidActions)
                
                //set texture, size, and position of empire state building and animate to that state
                node.texture = SKTexture(imageNamed: "EmpireState")
                let moveMoon = SKAction.move(to: CGPoint(x: 200, y: 90), duration: 0.5)
                let resizeMoon = SKAction.resize(toWidth: 10, height: 35, duration: 0.5)
                let moonActions = SKAction.group([moveMoon, resizeMoon])
                node.run(moonActions)
                
                //move magnifyed asteroid and magnifying glass off screen bc it is not used
                asteroidBigNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                let moveBigAsteroid = SKAction.move(to:CGPoint(x: -100, y: -100), duration:0.5)
                asteroidBigNode.run(moveBigAsteroid)
                let moveMagnifying = SKAction.move(to: CGPoint(x: -100, y: 100), duration: 0.5)
                magnifyingGlass.run(moveMagnifying)
            }
                
            //state of Massachusetts selected
            else if(objectNode.objectName == "Massachusetts") {
                
                //clear magnifying line to prepare redraw
                line.removeFromParent()
                
                //set position and size of asteroid and animate to that state
                let moveAsteroid = SKAction.move(to: CGPoint(x: 54, y: 286), duration: 0.5)
                let resizeAsteroid = SKAction.resize(toWidth: 265, height: 237, duration: 0.5)
                let asteroidActions = SKAction.group([moveAsteroid, resizeAsteroid])
                asteroidNode.run(asteroidActions)
                
                //set texture, size, and position of massachusets and animate to that state
                node.texture = SKTexture(imageNamed: "Massachusetts")
                let moveMoon = SKAction.move(to: CGPoint(x: 40, y: 520), duration: 0.5)
                let resizeMoon = SKAction.resize(toWidth: 280, height: 173, duration: 0.5)
                let moonActions = SKAction.group([moveMoon, resizeMoon])
                node.run(moonActions)
                
                //move magnifyed asteroid and magnifying glass off screen bc its not needed
                asteroidBigNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                let moveBigAsteroid = SKAction.move(to:CGPoint(x: -100, y: -100), duration:0.5)
                asteroidBigNode.run(moveBigAsteroid)
                //                asteroidBigNode.anchorPoint = CGPoint(x:0, y:1)
                let moveMagnifying = SKAction.move(to: CGPoint(x: -100, y: 100), duration: 0.5)
                magnifyingGlass.run(moveMagnifying)
            }
            // United states selected
            else if(objectNode.objectName == "United States") {
                
                //clear magnifying line to prepare redraw
                line.removeFromParent()
                
                //set position and size of asteroid and move to that state
                let moveAsteroid = SKAction.move(to: CGPoint(x: 179, y: 286), duration: 0.5)
                let resizeAsteroid = SKAction.resize(toWidth: 23, height: 20, duration: 0.5)
                let asteroidActions = SKAction.group([moveAsteroid, resizeAsteroid])
                asteroidNode.run(asteroidActions)
                
                //set texture, position, and size of United states and move to that state
                node.texture = SKTexture(imageNamed: "UnitedStates")
                let moveMoon = SKAction.move(to: CGPoint(x: 11, y: 536), duration: 0.5)
                let resizeMoon = SKAction.resize(toWidth: 350, height: 207, duration: 0.5)
                let moonActions = SKAction.group([moveMoon, resizeMoon])
                node.run(moonActions)
                
                //move magnified asteroid and magnifying glass in position
                asteroidBigNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                let moveBigAsteroid = SKAction.move(to:CGPoint(x: 171, y: 150), duration:0.5)
                asteroidBigNode.run(moveBigAsteroid)
                let moveMagnifying = SKAction.move(to: CGPoint(x: 171, y: 150), duration: 0.5)
                magnifyingGlass.run(moveMagnifying)
                var points = [CGPoint(x: 171, y: 150), CGPoint(x: 192, y: 276)]
                line = SKShapeNode(points: &points, count: points.count)
                line.lineWidth = 3
                addChild(line)
                
            }
        }
    }
    

}

