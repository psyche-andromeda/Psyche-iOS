//
//  ImageCollectionViewCell.swift
//  Psyche
//
//  Created by Kevin Shim on 2/23/18.
//  Copyright © 2018 Psyche Andromeda. All rights reserved.
//

import UIKit

//collectionview cell used to display images on social media grid view and gallery 

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
}
