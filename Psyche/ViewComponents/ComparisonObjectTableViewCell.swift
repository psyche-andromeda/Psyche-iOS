//
//  ObjectTableViewCell.swift
//  ParallaxPageView
//
//  Created by Jan Chen on 3/15/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

//collectionview cell that displays comparison objects below the scene

class ComparisonObjectTableViewCell: UICollectionViewCell {
    
    @IBOutlet weak var objectImageView: UIImageView!
    @IBOutlet weak var objectNameLabel: UILabel!
    
    var comparisonObject: ComparisonObject? {
        didSet {
            self.displayObject() //set image and text for collectionview cell
        }
    }
    
    private func displayObject() {
        if let comparisonObject = comparisonObject {
            objectImageView.image = comparisonObject.objectIcon
            objectNameLabel.text = comparisonObject.objectName
        }
        else {
            objectNameLabel.text = ""
        }
    }
}
