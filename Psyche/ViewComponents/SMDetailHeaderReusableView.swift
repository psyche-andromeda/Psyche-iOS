//
//  SMDetailHeaderReusableView.swift
//  Psyche
//
//  Created by Kevin Shim on 2/16/18.
//  Copyright © 2018 Psyche Andromeda. All rights reserved.
//

import UIKit

//header view for social media detail

class SMDetailHeaderReusableView: UICollectionReusableView {
        
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var postTextLabel: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var smLogoImageView: UIImageView!
    @IBOutlet weak var tagLineLabel: UILabel!
}
