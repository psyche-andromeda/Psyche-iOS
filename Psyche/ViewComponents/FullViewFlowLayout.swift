//
//  FullViewFlowLayout.swift
//  ParallaxPageView
//
//  Created by Kevin Shim on 1/19/18.
//  Copyright © 2018 Kevin Shim. All rights reserved.
//

import UIKit

class FullViewFlowLayout: UICollectionViewFlowLayout {
    override var collectionViewContentSize : CGSize {
        
        let count = self.collectionView?.dataSource?.collectionView(self.collectionView!, numberOfItemsInSection: 0)
        let canvasSize = self.collectionView!.frame.size
        var contentSize = canvasSize
        
        //set content size for vertical scrolling
        if self.scrollDirection == UICollectionViewScrollDirection.vertical {
            let rowCount = Int((canvasSize.height - self.itemSize.height) / (self.itemSize.height + self.minimumInteritemSpacing) + 1)
            
            let page = ceilf(Float(count!) / Float(rowCount))
            contentSize.height = CGFloat(page) * canvasSize.height
        }
        
        //set content size for horizontal scrolling
        else if self.scrollDirection == UICollectionViewScrollDirection.horizontal{
            
            let columnCount = Int((canvasSize.width - self.itemSize.width) / (self.itemSize.width + self.minimumLineSpacing) + 1)
            let page = ceilf(Float(count!) / Float(columnCount))
            contentSize.width = CGFloat(page) * canvasSize.width
        }
        return contentSize
    }
    
    func frameForItemAtIndexPath(_ indexPath: IndexPath) -> CGRect {
        //set frame for page to fill the size of collection view so rotation is supported
        
        self.itemSize = self.collectionView!.frame.size
        let canvasSize = self.collectionView!.frame.size
        let rowCount = Int((canvasSize.height - self.itemSize.height) / (self.itemSize.height + self.minimumInteritemSpacing) + 1)
        let columnCount = Int((canvasSize.width - self.itemSize.width) / (self.itemSize.width + self.minimumLineSpacing) + 1)
        
        let page = Int(CGFloat(indexPath.row) / CGFloat(rowCount * columnCount))
        
        var cellFrame = CGRect.zero
        cellFrame.origin.x = 0
        cellFrame.origin.y = 0
        cellFrame.size.width = self.itemSize.width
        cellFrame.size.height = self.itemSize.height
        
        if self.scrollDirection == UICollectionViewScrollDirection.vertical {
            cellFrame.origin.y += CGFloat(page) * canvasSize.height
        } else if self.scrollDirection == UICollectionViewScrollDirection.horizontal{
            cellFrame.origin.x += CGFloat(page) * canvasSize.width
        }
        
        return cellFrame
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        //set layout for each cell
        let attr = super.layoutAttributesForItem(at: indexPath)?.copy() as! UICollectionViewLayoutAttributes?
        attr!.frame = self.frameForItemAtIndexPath(indexPath)
        return attr
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let originAttrs = super.layoutAttributesForElements(in: rect)
        var attrs: [UICollectionViewLayoutAttributes]? = Array<UICollectionViewLayoutAttributes>()
        
        for attr in originAttrs! {
            let idxPath = attr.indexPath
            let itemFrame = self.frameForItemAtIndexPath(idxPath)
            if itemFrame.intersects(rect) {
                let nAttr = self.layoutAttributesForItem(at: idxPath)
                attrs?.append(nAttr!)
            }
        }
        
        return attrs
    }
}
