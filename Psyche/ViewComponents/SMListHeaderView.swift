//
//  listHeaderView.swift
//  Psyche
//
//  Created by Kevin Shim on 12/14/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit

//headerview for list feed

class SMListHeaderView: UICollectionReusableView {
    @IBOutlet weak var sourceSegmentedControl: UISegmentedControl!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //set segmented control action handler to change social media source
        sourceSegmentedControl.addTarget(self, action: #selector(SMListHeaderView.segmentedControlValueChanged), for: .valueChanged)
    }
    
    @objc func segmentedControlValueChanged(segmentedControl: UISegmentedControl){
        if segmentedControl.selectedSegmentIndex == 2{ //if facebook is selected open app or browser
            guard let url = URL(string: "https://www.facebook.com/NASAPsyche") else {
                return //be safe
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            segmentedControl.selectedSegmentIndex = 1
        }
        if segmentedControl.selectedSegmentIndex == 3{ //if instagram is selected open app or browswer
            guard let url = URL(string: "https://www.instagram.com/nasapsyche/") else {
                return //be safe
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            segmentedControl.selectedSegmentIndex = 1
        }
    }
    
}
