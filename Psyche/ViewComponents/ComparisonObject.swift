//
//  ComparisonObject.swift
//  ParallaxPageView
//
//  Created by Jan Chen on 3/15/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class ComparisonObject {
    
    var objectIcon: UIImage!
    var objectImage: String
    var objectName: String
    var comparisonTitle: String

    
    
    init(objectIcon: UIImage, objectImage: String, objectName: String, comparisonTitle: String) { //initialize comparison object
        self.objectIcon = objectIcon
        self.objectImage = objectImage
        self.objectName = objectName
        self.comparisonTitle = comparisonTitle
    }
    
    static func getComparisonObjects() -> [ComparisonObject] { //list of comparison objects
        return [
            ComparisonObject(objectIcon: #imageLiteral(resourceName: "earth_icon"), objectImage: "Earth", objectName: "Earth", comparisonTitle: "Psyche and Earth"),
            ComparisonObject(objectIcon: #imageLiteral(resourceName: "moon_icon"), objectImage: "Moon", objectName: "Moon", comparisonTitle: "Psyche and Moon"),
//            ComparisonObject(objectIcon: #imageLiteral(resourceName: "car_icon"), objectImage: "Car", objectName: "Car", comparisonTitle: "Psyche and Car"),
            ComparisonObject(objectIcon: #imageLiteral(resourceName: "mass_icon"), objectImage: "Massachusetts", objectName: "Massachusetts", comparisonTitle: "Psyche and Massachusetts"),
            ComparisonObject(objectIcon: #imageLiteral(resourceName: "empire_icon"), objectImage: "EmpireState", objectName: "Empire State", comparisonTitle: "Psyche and Empire State Building"),
            ComparisonObject(objectIcon: #imageLiteral(resourceName: "us_icon"), objectImage: "UnitedStates", objectName: "United States", comparisonTitle: "Psyche and United States")
        ]
    }
    
}
