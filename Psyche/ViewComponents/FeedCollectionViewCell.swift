//
//  FeedCollectionViewCell.swift
//  Psyche
//
//  Created by Kevin Shim on 12/9/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import UIKit

//collectionview cell for social media list feed

class FeedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var postTextLabel: UITextView!
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageBottomSpacing: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var smLogoImageView: UIImageView! //facebook or twitter icon
    @IBOutlet weak var tagLineLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code - set cell dimentions and borders to fit branding
        layer.cornerRadius = 7
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width-54
        cellWidth.constant = screenWidth
        imageHeight.constant -= 200
        postTextLabel.textContainerInset = .zero
        postTextLabel.contentInset = UIEdgeInsetsMake(0, -5, 0, 0)
    }

}
