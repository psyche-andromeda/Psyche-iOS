//
//  Slideshow.swift
//  ParallaxPageView
//
//  Created by Jan Chen on 2/9/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class Slideshow {
    
    var sectionTitle: String
    var slideTitle: String
    var slideDescription: String
    var slideImage: UIImage!
    var slideIndex: Int
    
    init(sectionTitle: String, slideTitle: String, slideDescription: String, slideImage: UIImage, slideIndex: Int) {
        self.sectionTitle = sectionTitle
        self.slideTitle = slideTitle
        self.slideDescription = slideDescription
        self.slideImage = slideImage
        self.slideIndex = slideIndex
    }
    
    static func getAsteroidFacts() -> [Slideshow]{
        return [
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 1 CONTENT",
                      slideDescription: "Psyche is dense perhaps as dense as seven thousand kilograms per cubic meter similar to the density of a bar of steel",
                      slideImage: UIImage(named: "Density")!,
                      slideIndex: 0),
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 1 CONTENT",
                      slideDescription: "Psyche is dense perhaps as dense as seven thousand kilograms per cubic meter similar to the density of a bar of steel",
                      slideImage: UIImage(named: "HowWasPsycheFormed")!,
                      slideIndex: 1)
            
        ]
    }
    static func getSpacecraftFacts() -> [Slideshow]{
        return [
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 1 CONTENT",
                      slideDescription: "the psyche spacecraft is comprised of the bus or body, two soloar arrays in a cross formation and the instrument payload. The psyche spacecraft including the solar panels is about the size of a singles tennis court",
                      slideImage: UIImage(named: "Spacecraft")!,
                      slideIndex: 0),
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 2 CONTENT",
                      slideDescription: "The psyche spacecraft will use solar electric (low-thrust) propulsion to travel to and orbit the asteroid. The spacecraft will include a Gamma Ray and Neutron Spectrometer, a Multispectral Imager, a Magnetometer and an x-band radio telecommunications system. The psyche mission will also test a new laser communication technology, called deep space optical communication",
                      slideImage: UIImage(named: "SpacecraftPart")!,
                      slideIndex: 1),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "PROPULSION SYSTEM",
                      slideDescription: "The spacecraft will be propelled by solar electric propulsion. Solar electric propulsion uses electricity from solar arrays to create electromagnetic fields to accelerate and expel charged atoms (ions) of Xenon to create a very low thrust with a very efficient use of propellant.",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 2),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "MULTISPECTRAL IMAGER",
                      slideDescription: "The Multispectral Imager provides high-resolution images using filters to discriminate between Psyche’s metallic and silicate constituents. The instrument consists of a pair of identical cameras designed to acquire geologic, compositional, and topographic data. ",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 3),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "GAMMA RAY AND NEUTRON SPECTROMETER",
                      slideDescription: "The Gamma Ray and Neutron Spectrometer will detect, measure, and map Psyche’s elemental composition. It is mounted on a 2-m boom to distance the sensors from background radiation created by energetic particles interacting with the spacecraft and provides an unobstructed field of view. ",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 4),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "MAGNETOMETER",
                      slideDescription: "The Psyche Magnetometer is designed to detect and measurethe remnant magnetic field of the asteroid. It is composed of two identical high-sensitivity magnetic field sensors located at the middle and outer end of a 2-m (6-foot) boom.",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 5),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "RADIO SCIENCE",
                      slideDescription: "The Psyche mission will use the X-band radio telecommunications system to measure Psyche’s gravity field to high precision. When combined with topography derived from onboard imagery, this will provide information on the interior structure of Psyche.",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 6),
            Slideshow(sectionTitle: "Spacecraft",
                      slideTitle: "DEEP SPACE OPTICAL COMMUNICATION (DSOC)",
                      slideDescription: "The Psyche mission will test a  sophisticated new laser  communication technology that  encodes data in photons (rather  than radio waves) to communicate  between a probe in deep space  and Earth.",
                      slideImage: UIImage(named: "SpacecraftDiagram")!,
                      slideIndex: 7),
            
        ]
    }
    static func getMissionFacts() -> [Slideshow]{
        return [
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 1 CONTENT",
                      slideDescription: "What is Psyche? Psyche is both the name of an asteroid orbiting the sun between mars and jupiter and the name of the space mission to visit and study the asteroid",
                      slideImage: UIImage(named: "WhatIsPsyche")!,
                      slideIndex: 0),
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 2 CONTENT",
                      slideDescription: "The Psyche Mission costa total of $850 million not including the cost for launch service, deep space optical communications demonstration hardware or operations",
                      slideImage: UIImage(named: "Cost")!,
                      slideIndex: 1),
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 2 CONTENT",
                      slideDescription: "Why go to psyche? The asteroid is believed to be the metallic core of an early planet. We can never go to the Earth's core becasue it is about eighteen hundred miles below the surface, 3 million times the pressure of the atmosphere and 9 thousand degrees",
                      slideImage: UIImage(named: "whygotopsyche")!,
                      slideIndex: 2),
            Slideshow(sectionTitle: "null",
                      slideTitle: "SLIDE 3 CONTENT",
                      slideDescription: "Other science goals are, determine whether Psyche is a core, or if it is unmelted material, Determine the relative ages of regions of psyche's surface and characterize psyche's morphology, determine whether psyche was formed under conditions more oxidizing or reducing than earth's core, and determine whether small metal bodies incorporate the same light elements as are expected in the earth's high pressure core",
                      slideImage: UIImage(named: "OtherGoals")!,
                      slideIndex: 3)
            
        ]
    }
    

}
