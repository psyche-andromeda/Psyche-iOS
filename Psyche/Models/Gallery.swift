//
//  Gallery.swift
//  Psyche
//
//  Created by Kevin Shim on 4/18/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import Foundation
import UIKit
class Picture{
    var image = UIImage()
    init(n: String){
        self.image = UIImage(named: n)!
    }
}

class Gallery{
    static var gallery : [Picture] = []
    
    static func getGallery() ->[Picture] {
        return [Picture(n: "3DModel"),
                Picture(n: "PsychePaperQuilling"),
                Picture(n: "AsteroidBelt"),
                Picture(n: "LampShade"),
                Picture(n: "PsychePerspective"),
                Picture(n: "Comic"),
                Picture(n: "ManyMinds"),
                Picture(n: "PsychePoem"),
                Picture(n: "ComingIn2022"),
                Picture(n: "MylarPsyche"),
                Picture(n: "PsycheStopMotion"),
                Picture(n: "ConceptionOfPsyche"),
                Picture(n: "PopUpBook"),
                Picture(n: "PsycheZine"),
                Picture(n: "Construction"),
                Picture(n: "Psyche3DBook"),
                Picture(n: "PsychetheGodess"),
                Picture(n: "HeavyMetalLightShow"),
                Picture(n: "PsycheCrossStitch"),
                Picture(n: "SurrealPsyche"),
                Picture(n: "Interns"),
                Picture(n: "PsycheEyeChart"),
                Picture(n: "Telescope"),
                Picture(n: "Iron"),
                Picture(n: "PsycheOutoftheDark"),
                Picture(n: "WordCloud")]
    }
}
