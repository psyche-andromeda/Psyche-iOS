//
//  SMPosts.swift
//  Psyche
//
//  Created by Kevin Shim on 12/27/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import Foundation
import Alamofire

extension NSMutableAttributedString {
    func convertHtmlSymbols() {
        
        let char_dictionary = [
            "&amp;" : "&",
            "&lt;" : "<",
            "&gt;" : ">",
            "&quot;" : "\"",
            "&apos;" : "'"
        ];
        for (escaped_char, unescaped_char) in char_dictionary {
            self.mutableString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: NSMakeRange(0, self.mutableString.length))
        }
        
    }
}

enum SMType {
    case twitter
    case instagram
    case facebook
    case blog
    case embed
}

class SMPost{
    var name : String
    var handle : String
    var profileImageLink : String
    var postDate : Date
    var post : NSMutableAttributedString
    var postImageLinks: [String]
    var postVideoLinks : [String]
    var quotedPost : SMPost?
    var repostedPost : SMPost?
    var goToLink : String?
    var source : SMType

    init(fullName: String, userHandle: String, profileImgLink: String,  date: Date, text: NSMutableAttributedString, postImgLink: [String], videoLink: [String], quoted_post: SMPost?, reposted_post: SMPost?, type: SMType){
        name = fullName
        handle = userHandle
        profileImageLink = profileImgLink
        postDate = date
        postImageLinks = postImgLink
        postVideoLinks = videoLink
        quotedPost = quoted_post
        repostedPost = reposted_post
        post = text
        post.convertHtmlSymbols()
        source = type
    }
}

extension SMPost : Comparable{
    static func < (lhs: SMPost, rhs: SMPost) -> Bool {
        return lhs.postDate > rhs.postDate
    }
    
    static func == (lhs: SMPost, rhs: SMPost) -> Bool {
        return lhs.postDate == rhs.postDate
    }
}

public struct OrderedList<T: Comparable>: Sequence{
    
    let sort = true 
    
    fileprivate var array = [T]()
    
    public init() {
        self.array = [T]()
    }
    
    public var isEmpty: Bool {
        return array.isEmpty
    }
    
    public var count: Int {
        return array.count
    }
    
    public subscript(index: Int) -> T {
        return array[index]
    }
    
    public mutating func removeAtIndex(index: Int) -> T {
        return array.remove(at: index)
    }
    
    public mutating func removeAll() {
        array.removeAll()
    }
    
    public mutating func insert(_ newElement: T) {
        let i = findInsertionPoint(newElement)
        array.insert(newElement, at: i)
        
    }
    
    private func findInsertionPoint(_ newElement: T) -> Int {
        
        if !sort{
            return array.count
        }
        
        for i in 0..<array.count {
            if newElement <= array[i] {
                return i
            }
        }
        return array.count  // insert at the end
    }
    public func makeIterator() -> OrderedList<T>.Iterator {
        return OrderedList<T>.Iterator(self)
    }
    
    public struct Iterator: IteratorProtocol{
        
        var index = 0;
        let orderedList: OrderedList<T>
        
        init(_ orderedList: OrderedList<T>) {
            self.orderedList = orderedList
        }
        
        public mutating func next() -> T? {
            if index >= (orderedList.count - 1){
                return nil
            }
            
            index += 1
            return orderedList[index]
        }
    }
}

class SMPosts {
    
    static var posts = OrderedList<SMPost>();
    static var loadedFB = false;
    static var loadedTW = false;
    static var loading = false;
    
    static func getAllPosts() -> OrderedList<SMPost>{
        while (!loadedFB || !loadedTW){
            
        }
        return posts
    }
    
    static func getFBPosts() -> OrderedList<SMPost>{
        var fbPosts = OrderedList<SMPost>()
        while(!loadedFB){
            
        }
        for post in posts{
            if post.source == .facebook{
                fbPosts.insert(post)
            }
        }
        return fbPosts
    }
    
    static func getTWPosts() -> OrderedList<SMPost>{
        
        loading = true
        var twPosts = OrderedList<SMPost>()
        
        while(!loadedTW){
            
        }
        for post in posts{
            if post.source == .twitter{
                twPosts.insert(post)
            }
        }
        loading = false
        return twPosts
    }
    
    static func loadPosts (completionHandler: @escaping () -> Void){
        
        loadedTW = false
        loadedFB = false
        posts.removeAll()
        
        if !loadedFB{
            getFacebookPosts {
                loadedFB = true
                if loadedTW{
                    completionHandler()
                }
            }
        }
        if !loadedTW{
            getTwitterPosts {
                loadedTW = true
                if loadedFB{
                    completionHandler()
                }
            }
        }
        
        completionHandler()
        
    }
    
    static func getFacebookPosts(completionHandler: @escaping () -> Void){
        
        let pageID = "1598743977091187"
        let accessToken = "103575667102349|7q1t0bV8K7icZuDqn0wBVURB6Z0"
        
        let url = "https://graph.facebook.com/v2.11/\(pageID)?access_token=\(accessToken)&fields=posts{message,attachments,created_time},picture.type(large),username,name"
        
        let encodedURl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        Alamofire.request(encodedURl).responseJSON(completionHandler: {response in
            //print(response)
            let resultBody = response.result.value as! [String: Any]
            
            let pictureData = resultBody["picture"] as! [String : Any]
            let pictureInfo = pictureData["data"] as! [String : Any]
            let profilepictureUrl = pictureInfo["url"] as! String
            
            let username = resultBody["username"] as! String
            let fullNmae = resultBody["name"] as! String
            
            let postsData = resultBody["posts"] as! [String : Any]
            let posts = postsData["data"] as! [Any]
            for postObj in posts {
                let post = postObj as! [String : Any]
                
                let postText = NSMutableAttributedString(string: post["message"] as! String, attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 14)!])
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                let postDate = dateFormatter.date(from: post["created_time"]! as! String)!
                
                var imgLinks : [String] = []
                var quotedPost : SMPost?
                
                if let attatchmentDataObj = post["attachments"] as? [String : Any]{
                    let attatchmentData = attatchmentDataObj["data"] as! [Any]
                    
                    for attachmentObj in attatchmentData{
                        let attachment = attachmentObj as! [String : Any]
                        let type = attachment["type"]  as! String
                        
                        if type == "photo"{
                            let media = attachment["media"] as! [String : Any]
                            let image = media["image"] as! [String : Any]
                            imgLinks.append(image["src"] as! String)
                        } else if type == "share"{
                            let title = attachment["title"] as! String
                            var description = ""
                            if let descriptionText = attachment["description"] as? String{
                                description = descriptionText
                            }
                            
                            let mediaInfo = attachment["media"] as! [String : Any]
                            let imageInfo = mediaInfo["image"] as! [String : Any]
                            var imageLinks : [String] = []
                            imageLinks.append(imageInfo["src"] as! String)
                            
                            let targetInfo = attachment["target"] as! [String : Any]
                            let targetUrl = targetInfo["url"] as! String
                            
                            quotedPost = SMPost(fullName: title, userHandle: "", profileImgLink: "", date: Date(), text: NSMutableAttributedString(string: description), postImgLink: imageLinks, videoLink: [], quoted_post: nil, reposted_post: nil, type: .embed)
                            
                            quotedPost!.goToLink = targetUrl
                        }
                    }
                }
                
                self.posts.insert(SMPost(fullName: fullNmae, userHandle: username, profileImgLink: profilepictureUrl, date: postDate, text: postText, postImgLink: imgLinks, videoLink: [], quoted_post: quotedPost, reposted_post: nil, type: SMType.facebook))
                
                
                
            }
            
            completionHandler()
            
        });
    }
    
    static func getTwitterPosts(completionHandler: @escaping () -> Void){
        if loading{
            completionHandler()
        }
        
        loading = true
        
        Alamofire.request("https://api.twitter.com/1.1/search/tweets.json?q=from%3ANASAPsyche&tweet_mode=extended", headers: ["Authorization" : "Bearer AAAAAAAAAAAAAAAAAAAAADw23wAAAAAAcYCmKsnbTZjGywm8a2icDoQmDSo%3D39EfxsoEOa8lkQ9FjgKOpTVqPVdRdsMF3Kb9ufZOqXF2D3JCfR"]).responseJSON{ response in

            let resultBody = response.result.value as! [String: Any]
            let statusArray = resultBody["statuses"]! as? [Any]
            for status in statusArray! {
                let statusDictionary = status as! [String: Any]

                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E MMM d HH:mm:ss Z yyyy"
                let date = dateFormatter.date(from: statusDictionary["created_at"]! as! String)!

                let postRange = statusDictionary["display_text_range"]! as! [Int]
                let fullText = statusDictionary["full_text"]! as! String
                let postTextFull = fullText.replacingOccurrences(of: "…", with: "...")
                print(postTextFull.unicodeScalars.count)
                let postText = String(postTextFull.unicodeScalars[postTextFull.unicodeScalars.index(postTextFull.unicodeScalars.startIndex, offsetBy: postRange[0])..<postTextFull.unicodeScalars.index(postTextFull.unicodeScalars.startIndex, offsetBy: postRange[1])])
                let attributedPost = NSMutableAttributedString(string: postText, attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 14)!])
                
                let entities = statusDictionary["entities"]! as! [String:Any]
                if let userMentions = entities["user_mentions"] as? [Any]{
                    for mentionedUser in userMentions{
                        let mentioned = mentionedUser as! [String:Any]
                        let indeces = mentioned["indices"]! as! [Int]
                        if indeces[0] < postRange[0] || indeces[0] > postRange[1]{
                            continue
                        }
                        attributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                        attributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                    }
                }
                if let hashTags = entities["hashtags"] as? [Any]{
                    for hashtag in hashTags{
                        let hash = hashtag as! [String:Any]
                        let indeces = hash["indices"]! as! [Int]
                        if indeces[0] < postRange[0] || indeces[0] > postRange[1]{
                            continue
                        }
                        attributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                        attributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                    }
                }
                if let urlTags = entities["urls"] as? [Any]{
                    for urltag in urlTags{
                        let url = urltag as! [String:Any]
                        let indeces = url["indices"]! as! [Int]
                        if indeces[0] < postRange[0] || indeces[0] > postRange[1]{
                            continue
                        }
                        attributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                        attributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-postRange[0], indeces[1]-indeces[0]))
                    }
                }

                let user = statusDictionary["user"] as! [String:Any]
                let userName = (user["name"]!) as! String
                let userScreenName = (user["screen_name"]!) as! String
                let userImgUrl = (user["profile_image_url_https"]!) as! String
                
                var imgLinks : [String] = []
                var videoLinks : [String] = []
                
                if let extEntities = statusDictionary["extended_entities"] as? [String: Any]{
                    let mediaItems = extEntities["media"] as! [Any]
                    for media in mediaItems{
                        let mediaDictionary = media as! [String: Any]
                        let typeString = mediaDictionary["type"] as! String
                        if typeString == "photo"{
                            imgLinks.append(mediaDictionary["media_url_https"] as! String)
                        } else if typeString == "video"{
                            videoLinks.append(mediaDictionary["media_url_https"] as! String)
                        }
                    }
                }
                
                var retweetedPost : SMPost?
                var quotedPost : SMPost?
                
                if let retweeted = statusDictionary["retweeted_status"] as? [String: Any]{
                    
                    let ruser = retweeted["user"] as! [String:Any]
                    let ruserName = (ruser["name"]!) as! String
                    let ruserScreenName = (ruser["screen_name"]!) as! String
                    let rprofileImgUrl = (ruser["profile_image_url_https"]!) as! String
                    let rdate = dateFormatter.date(from: retweeted["created_at"]! as! String)!
                    let rpostRange = retweeted["display_text_range"]! as! [Int]
                    var rfullText = ((retweeted["full_text"]!) as! String)
                    rfullText = rfullText.replacingOccurrences(of: "…", with: "...")
                    
                    let rpostText = String(rfullText.unicodeScalars[rfullText.unicodeScalars.index(rfullText.unicodeScalars.startIndex, offsetBy: rpostRange[0])..<rfullText.unicodeScalars.index(rfullText.unicodeScalars.startIndex, offsetBy: rpostRange[1])])
                    
                    var rimgLinks : [String] = []
                    var rvideoLinks : [String] = []
                    var rquotedPost : SMPost?
                    
                    if let rextEntities = retweeted["extended_entities"] as? [String: Any]{
                        let mediaItems = rextEntities["media"] as! [Any]
                        for media in mediaItems{
                            let mediaDictionary = media as! [String: Any]
                            let typeString = mediaDictionary["type"] as! String
                            if typeString == "photo"{
                                rimgLinks.append(mediaDictionary["media_url_https"] as! String)
                            } else if typeString == "video"{
                                rvideoLinks.append(mediaDictionary["media_url_https"] as! String)
                            }
                        }
                    }
                    
                    if let quoted = retweeted["quoted_status"] as? [String: Any]{
                        
                        let quser = quoted["user"] as! [String:Any]
                        let quserName = (quser["name"]!) as! String
                        let quserScreenName = (quser["screen_name"]!) as! String
                        let qprofileImgUrl = (quser["profile_image_url_https"]!) as! String
                        let qdate = dateFormatter.date(from: quoted["created_at"]! as! String)!
                        let qpostRange = quoted["display_text_range"]! as! [Int]
                        var qfullText = ((quoted["full_text"]!) as! String)
                        qfullText = qfullText.replacingOccurrences(of: "…", with: "...")
                        let qpostText = String(qfullText.unicodeScalars[qfullText.unicodeScalars.index(qfullText.unicodeScalars.startIndex, offsetBy: qpostRange[0])..<qfullText.unicodeScalars.index(qfullText.unicodeScalars.startIndex, offsetBy: qpostRange[1])])
                        
                        
                        var qimgLinks : [String] = []
                        var qvideoLinks : [String] = []
                        
                        if let qextEntities = quoted["extended_entities"] as? [String: Any]{
                            let mediaItems = qextEntities["media"] as! [Any]
                            for media in mediaItems{
                                let mediaDictionary = media as! [String: Any]
                                let typeString = mediaDictionary["type"] as! String
                                if typeString == "photo"{
                                    qimgLinks.append(mediaDictionary["media_url_https"] as! String)
                                } else if typeString == "video"{
                                    qvideoLinks.append(mediaDictionary["media_url_https"] as! String)
                                }
                            }
                        }
                        
                        
                        
                        let qAttributedPost = NSMutableAttributedString(string: qpostText, attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 14)!])
                        
                        let qentities = quoted["entities"]! as! [String:Any]
                        if let quserMentions = qentities["user_mentions"] as? [Any]{
                            for mentionedUser in quserMentions{
                                let mentioned = mentionedUser as! [String:Any]
                                let indeces = mentioned["indices"]! as! [Int]
                                if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                    continue
                                }
                                qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                                qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            }
                        }
                        if let qhashTags = qentities["hashtags"] as? [Any]{
                            for hashtag in qhashTags{
                                let hash = hashtag as! [String:Any]
                                let indeces = hash["indices"]! as! [Int]
                                if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                    continue
                                }
                                qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                                qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            }
                        }
                        
                        if let qurlTags = qentities["urls"] as? [Any]{
                            for urltag in qurlTags{
                                let url = urltag as! [String:Any]
                                let indeces = url["indices"]! as! [Int]
                                if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                    continue
                                }
                                qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                                qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            }
                        }
                        
                        rquotedPost = SMPost(fullName: quserName, userHandle: "@\(quserScreenName)", profileImgLink: qprofileImgUrl, date: qdate, text: qAttributedPost, postImgLink: qimgLinks, videoLink: qvideoLinks, quoted_post: nil, reposted_post: nil, type: SMType.twitter)
                        
                    }
                    let rAttributedPost = NSMutableAttributedString(string: rpostText, attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 14)!])
                    
                    let rentities = retweeted["entities"]! as! [String:Any]
                    if let ruserMentions = rentities["user_mentions"] as? [Any]{
                        for mentionedUser in ruserMentions{
                            let mentioned = mentionedUser as! [String:Any]
                            let indeces = mentioned["indices"]! as! [Int]
    //                        print(rAttributedPost.string)
                            if indeces[0] < rpostRange[0] || indeces[0] > rpostRange[1]{
                                continue
                            }
                            rAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-rpostRange[0], (indeces[1]-indeces[0])))
                            rAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-rpostRange[0], (indeces[1]-indeces[0])))
                        }
                    }
                    if let rhashTags = rentities["hashtags"] as? [Any]{
                        for hashtag in rhashTags{
                            let hash = hashtag as! [String:Any]
                            let indeces = hash["indices"]! as! [Int]
                            if indeces[0] < rpostRange[0] || indeces[0] > rpostRange[1]{
                                continue
                            }
                            rAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-rpostRange[0], indeces[1]-indeces[0]))
                            rAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-rpostRange[0], (indeces[1]-indeces[0])))
                        }
                    }
                    if let rurlTags = rentities["urls"] as? [Any]{
                        for urltag in rurlTags{
                            let url = urltag as! [String:Any]
                            let indeces = url["indices"]! as! [Int]
                            if indeces[0] < rpostRange[0] || indeces[0] > rpostRange[1]{
                                continue
                            }
                            rAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-rpostRange[0], indeces[1]-indeces[0]))
                            rAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-rpostRange[0], (indeces[1]-indeces[0])))
                        }
                    }
                    
                    retweetedPost = SMPost(fullName: ruserName, userHandle: "@\(ruserScreenName)", profileImgLink: rprofileImgUrl, date: rdate, text: rAttributedPost, postImgLink: rimgLinks, videoLink: rvideoLinks, quoted_post: rquotedPost, reposted_post: nil, type: SMType.twitter)
                    
                }
                
                if let quoted = statusDictionary["quoted_status"] as? [String: Any]{
                    
                    let quser = quoted["user"] as! [String:Any]
                    let quserName = (quser["name"]!) as! String
                    let quserScreenName = (quser["screen_name"]!) as! String
                    let qprofileImgUrl = (quser["profile_image_url_https"]!) as! String
                    let qdate = dateFormatter.date(from: quoted["created_at"]! as! String)!
                    let qpostRange = quoted["display_text_range"]! as! [Int]
                    var qfullText = ((quoted["full_text"]!) as! String)
                    qfullText = qfullText.replacingOccurrences(of: "…", with: "...")
                    let qpostText = String(qfullText.unicodeScalars[qfullText.unicodeScalars.index(qfullText.unicodeScalars.startIndex, offsetBy: qpostRange[0])..<qfullText.unicodeScalars.index(qfullText.unicodeScalars.startIndex, offsetBy: qpostRange[1])])
                    
                    var qimgLinks : [String] = []
                    var qvideoLinks : [String] = []
                    
                    if let qextEntities = quoted["extended_entities"] as? [String: Any]{
                        let mediaItems = qextEntities["media"] as! [Any]
                        for media in mediaItems{
                            let mediaDictionary = media as! [String: Any]
                            let typeString = mediaDictionary["type"] as! String
                            if typeString == "photo"{
                                qimgLinks.append(mediaDictionary["media_url_https"] as! String)
                            } else if typeString == "video"{
                                qvideoLinks.append(mediaDictionary["media_url_https"] as! String)
                            }
                        }
                    }
                    let qAttributedPost = NSMutableAttributedString(string: qpostText, attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 14)!])
                    
                    let qentities = quoted["entities"]! as! [String:Any]
                    if let quserMentions = qentities["user_mentions"] as? [Any]{
                        for mentionedUser in quserMentions{
                            let mentioned = mentionedUser as! [String:Any]
                            let indeces = mentioned["indices"]! as! [Int]
                            if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                continue
                            }
                            qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                        }
                    }
                    if let qhashTags = qentities["hashtags"] as? [Any]{
                        for hashtag in qhashTags{
                            let hash = hashtag as! [String:Any]
                            let indeces = hash["indices"]! as! [Int]
                            if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                continue
                            }
                            qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                        }
                    }
                    
                    if let qurlTags = qentities["urls"] as? [Any]{
                        for urltag in qurlTags{
                            let url = urltag as! [String:Any]
                            let indeces = url["indices"]! as! [Int]
                            if indeces[0] < qpostRange[0] || indeces[0] > qpostRange[1]{
                                continue
                            }
                            qAttributedPost.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "attributeColor")!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                            qAttributedPost.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "HelveticaNeue-Bold", size: 14)!, range: NSMakeRange(indeces[0]-qpostRange[0], indeces[1]-indeces[0]))
                        }
                    }
                    
                    quotedPost = SMPost(fullName: quserName, userHandle: "@\(quserScreenName)", profileImgLink: qprofileImgUrl, date: qdate, text: qAttributedPost, postImgLink: qimgLinks, videoLink: qvideoLinks, quoted_post: nil, reposted_post: nil, type: SMType.twitter)
                    
                }
                
                
                self.posts.insert(SMPost(fullName: userName, userHandle: "@\(userScreenName)", profileImgLink: userImgUrl, date: date, text: attributedPost, postImgLink: imgLinks, videoLink: videoLinks, quoted_post: quotedPost, reposted_post: retweetedPost, type: SMType.twitter))
                
                
            }
        loading = false
         completionHandler()
        }
        
        
        
    }
    
}
