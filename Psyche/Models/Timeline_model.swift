//
//  Timeline_model.swift
//  Psyche
//
//  Created by Ogheneochukome Erhueh on 12/21/17.
//  Copyright © 2017 Psyche Andromeda. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class Timeline_model
{
    var contManage:NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var milestones: [Timeline] = [] // this array will hold all of the contents fetched from core data
  
    // this fucntion get all objects stored in core data
    
    func get(index:Int) -> Timeline{
        return milestones[index]
    }
    
    func getData() -> [Timeline]
    {
        
        if milestones.count != 0{
            return milestones
        }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            
        
                addNewTMobject(name: "CONCEPT STUDY", phase: "A", desc: "In September 2015, the Psyche Mission is selected by NASA to develop a detailed concept study for consideration for NASA’s Discovery Program. The team, led by Lindy Elkins-Tanton, conducts the study and presents the proposed mission to NASA. A select group of science, technical, and industry experts review the details of the proposed mission, from concept and design, to execution and science application, as well as how the mission personnel from different institutions work together as a team. The panel then gives the mission their approval.", date: dateFormatterGet.date(from: "2015-09-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "PRELIMINARY DESIGN", phase: "B", desc: "The preliminary design of the spacecraft and its instruments is created by science and engineering teams working on the mission. In March 2019, the team undergoes project and flight system Preliminary Design Review. In May 2019, the team reaches Key Decision Point C, which gives them the official approval to move on to Phase C.", date: dateFormatterGet.date(from: "2017-01-01")! as NSDate, link: "", type: "Image")
            
                addNewTMobject(name: "CRITICAL DESIGN & BUILD", phase: "C", desc: "The science and engineering teams build the spacecraft and its instruments. In April 2020, the team undergoes a Project and Flight System Critical Design Review. The bus (body) of the spacecraft is completed in May 2020. In January 2021, the team conducts the Systems Integration Review to ensure that the system is ready to be integrated. The last step in Phase C is Key Decision Point D that gives the team the official approval to move on to Phase D.", date: dateFormatterGet.date(from: "2019-05-01")! as NSDate, link: "", type: "Image")
            
            addNewTMobject(name: "INSTRUMENT AND SPACECRAFT BUILD & ASSEMBLY", phase: "D", desc: "All the spacecraft subsystems are integrated onto the spacecraft bus. The spacecraft then undergoes vibration testing, environmental thermal-vacuum testing, and electromagnetic interference and compatibility testing. In May 2022, the team conducts the Operations Readiness Review to ensure the system, procedures, and all supporting software and personnel are ready and fully operational. Before launch, the team conducts Key Decision Point E that will determine readiness to conduct post launch operations.", date: dateFormatterGet.date(from: "2021-01-01")! as NSDate, link: "", type: "Image")
            
                addNewTMobject(name: "SPACECRAFT SHIPS TO LAUNCH SITE", phase: "D", desc: "The spacecraft, now fully assembled, ships to the launch site. The solar panels that extend from the body of the spacecraft are folded up to make it easier for transportation and launch.", date: dateFormatterGet.date(from: "2022-05-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "LAUNCH", phase: "D", desc: "At the launch site, the team conducts an entire re-check of the spacecraft before integration into the launch vehicle. The spacecraft is launched out of the Earth’s atmosphere and into space. The Post Launch Assessment Review is conducted before the mission moves on to Phase E.", date: dateFormatterGet.date(from: "2022-08-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "MARS GRAVITY ASSIST", phase: "E", desc: "The spacecraft uses the gravity of Mars to increase speed and set its trajectory to intersect with Psyche’s orbit around the Sun. The gravity assist is accomplished by entering and leaving the gravitational field of Mars which slingshots the spacecraft saving propellant, time, and expense.", date: dateFormatterGet.date(from: "2023-05-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "ARRIVAL AT PSYCHE", phase: "E", desc: "After 100 days in the approach phase, the spacecraft arrives at Psyche. The spacecraft measures the asteroid’s spin axis and rotation.", date: dateFormatterGet.date(from: "2026-01-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "ORBITING PSYCHE", phase: "E", desc: "The spacecraft orbits Psyche performing science operations from four different orbits, each successively closer to the asteroid. In each orbit, the instruments on board send data back to Earth to be analyzed by the mission’s science team.\n\nORBIT A:  CHARACTERIZATION – 59 Days (41 Orbits)\nORBIT B: TOPOGRAPHY – 76 Days (162 Orbits)\nORBIT C: GRAVITY SCIENCE – 100 Days (369 Orbits)\nORBIT D: ELEMENTAL MAPPING – 100 Days (585 Orbits)", date: dateFormatterGet.date(from: "2026-01-01")! as NSDate, link: "", type: "Image")
                addNewTMobject(name: "MISSION CLOSEOUT", phase: "F", desc: "The mission team provides all remaining deliverables and safely decommissions the space flight systems of the spacecraft.", date: dateFormatterGet.date(from: "2027-09-01")! as NSDate, link: "", type: "Image")
                
        
        
        return milestones
    
    }
    
    //This function will be used to add new object to the Timeline core data storge
    func addNewTMobject(name:String, phase:String, desc:String, date:NSDate, link:String, type:String)
    {
        let ent = NSEntityDescription.entity(forEntityName: "Timeline", in: self.contManage)
        let tmEvent: Timeline = Timeline(entity: ent!,insertInto: self.contManage) // create instance of Timeline entity
        
        //Set the values for all attributes
        tmEvent.name = name
        tmEvent.desc = desc
        tmEvent.phase = phase
        tmEvent.dateTime = date
        tmEvent.tmobj?.link = link
        tmEvent.tmobj?.type = type
        //save to core data
        
        milestones.append(tmEvent)
        
    }
    
    // This function will be used to delete all Timeline objects from core data
    func deleteTimeline(index: Int)
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Timeline")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try self.contManage.execute(deleteRequest)
        } catch {
            // TODO: handle the error
        }
        
    }
 
    
}


class gradientColor
{
   var grad: CAGradientLayer
    
    init() {
        let colorTop = UIColor(red:0.98, green:0.63, blue:0.00, alpha:1.0).cgColor
        let colorMiddle = UIColor(red:0.96, green:0.49, blue:0.20, alpha:1.0).cgColor
        let colorLast = UIColor(red:0.94, green:0.35, blue:0.40, alpha:1.0).cgColor
        grad = CAGradientLayer()
        grad.colors = [ colorTop, colorMiddle, colorLast]
        grad.locations = [ 0.0, 0.5, 1.0]
    }
}
