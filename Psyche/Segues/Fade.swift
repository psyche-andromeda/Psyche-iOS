//
//  Fade.swift
//  Psyche
//
//  Created by Kevin Shim on 4/11/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class Fade: UIStoryboardSegue {
    override func perform() {
        
        let firstVC = self.source.view as UIView?
        let secondVC = self.destination.view as UIView?
        
        
        
        secondVC?.alpha = 0.0
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVC!, aboveSubview: firstVC!)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.5, animations: { () -> Void in // set animation duration
            
            firstVC?.alpha = 0.0
            secondVC?.alpha = 1.0
            
        }) { (Finished) -> Void in
            
            self.source.navigationController?.pushViewController(self.destination, animated: false)
            firstVC?.alpha = 1.0
            
        }
    }
}
