//
//  FromTopToBottomSegue.swift
//  Psyche
//
//  Created by Kevin Shim on 4/10/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//

import UIKit

class FromTopToBottomSegue: UIStoryboardSegue {
    override func perform() {
        
        let firstVC = self.source.view as UIView?
        let secondVC = self.destination.view as UIView?
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        
        secondVC!.frame = CGRect(x: -0, y: -screenHeight, width: screenWidth, height: screenHeight)
       
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVC!, aboveSubview: firstVC!)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.3, animations: { () -> Void in // set animation duration
            
            firstVC?.frame = (firstVC?.frame)!.offsetBy(dx: 0.0, dy: screenHeight) // old screen stay
            
            secondVC?.frame = (secondVC?.frame)!.offsetBy(dx: 0.0, dy: screenHeight) // new screen strave from left to right
            
        }) { (Finished) -> Void in
            
            self.source.navigationController?.pushViewController(self.destination, animated: false)
            
        }
    }
}
