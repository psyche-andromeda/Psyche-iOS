# Psyche Andromeda

Psyche Andromeda is an iOS application developed for NASA's Psyche Mission.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Requirements

* iOS 11.0+ 
* Xcode 9.1+
* Swift 4.0+
* iPhone 6s or later

### Installing

Run pod install

Change bundle id and signing keys

## Built With

* [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP networking library
* [Kingfisher](https://github.com/onevcat/Kingfisher) - library for downloading and caching images from the web
 

## Authors

* **Kevin Shim** - *Developer*
* **Jan Chen** - *Developer*
* **Yifan Li** - *Developer*
* **Ogheneochukome Anastasia Erhueh** - *Developer*
* **Lauren Hancox** - *Project Manager*
* **Connor Keith** - *Graphic Designer*

## Acknowledgments

* [mohamede1945](https://github.com/mohamede1945/PagedHorizontalView) - PagedHorizontalView used in full screen collection views (horizontal and vertical)
* [Muhammad Naeem Jawaid](https://github.com/naeemjawaid/VerticalProgressView) - Vertical Progress Bar used in fact slides (horizontal and vertical)
* Apple Inc. - ARKit base

## About the mission

[NASA Website](https://www.nasa.gov)  
[Psyche Website](https://psyche.asu.edu)

### Mission Team
**Principal Investigator** - Lindy Elkins-Tanton

**Deputy Principal Investigator** - Jim Bell

#### Co-Investigators
Erik Asphaug  
David Bercovici  
Bruce Bills  
Richard Binzel  
William Bottke  
Ralf Jaumann  
Insoo Jun  
David Lawrence  
Simone Marchi  
Timothy McCoy  
Ryan Park  
Patrick Peplowski  
Thomas Prettyman  
Carol Raymond  
Chris Russell  
Benjamin Weiss  
Dan Wenkert  
Mark Wieczorek  
David Williams  
Maria Suber

#### Management Team
Diane Brown - Program Executive - NASA HQ  
Sarah Noble - Program Scientist - NASA HQ  
Belinda Wright - Mission Manager - NASA MSFC  
Henry Stone - Project Manager - JPL  
Robert Mase - Deputy Project Manager - JPL  
Deborah Bass - Mission System Manager - JPL  
Brian Johnson - Project Business Manager - JPL  
David Oh - Project System Engineer - JPL  
Kalyani Sukhatme - Payload Manager - JPL  
Rob Menke - Mission Assurance Manager - JPL  
Mark Brown - Flight System Manager - JPL  
Steve Scott - SEP Chassis Program Manager - SSL  
Peter Lord - SEP Chassis Deputy Program Manager - SSL  
Tess Calvert - ASU Project Manager - ASU  


#### Psyche Partners
Applied Physics Laboratory (APL)  
Arizona State University (ASU)  
Deutsches Zentrum für Luft- und Raumfahrt (DLR)  
Jet Propulsion Laboratory (JPL)  
Massachusetts Institute of Technology (MIT)  
Malin Space Science Systems (MSSS)  
Observatoire de la Côte d’Azur  
Planetary Science Institute (PSI)  
Smithsonian Institution  
Southwest Research Institute (SwRI)  
Space Systems Loral (SSL)  
University of Arizona  
University of California Los Angeles (UCLA)  
Yale University
