//
//  TM_Media+CoreDataProperties.swift
//  Psyche
//
//  Created by Kevin Shim on 4/11/18.
//  Copyright © 2018Kevin Shim All rights reserved.
//
//

import Foundation
import CoreData


extension TM_Media {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TM_Media> {
        return NSFetchRequest<TM_Media>(entityName: "TM_Media")
    }

    @NSManaged public var link: String?
    @NSManaged public var type: String?
    @NSManaged public var tm_media: Timeline?

}
